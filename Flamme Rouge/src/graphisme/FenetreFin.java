package graphisme;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import application.jeu.Jeu;

/**
 * @author GENY Vanessa
 *
 */
public class FenetreFin extends JFrame {

	// On recupere la dimension de l'ecran
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * @param j
	 *            jeu en cours
	 */
	public FenetreFin(Jeu j) {

		// On cree le panel principal
		JPanel container = new JPanel();

		// On lui attribut un manager de layout
		container.setLayout(new GridLayout(4, 1));

		// On le definit comme panel principal de la fenetre
		this.setContentPane(container);

		// Pour chaque joueur
		for (int i = 0; i < j.getTabj().size(); i++) {

			// On affiche sa position dans un label
			JLabel res = new JLabel("Position " + (i + 1) + " : "
					+ j.getTabj().get(i).getNom());
			// On centre le texte
			res.setAlignmentX(CENTER_ALIGNMENT);

			// On l'ajoute au panel
			container.add(res);
		}

		// On affiche le nombre de tours
		JLabel tours = new JLabel("Nombre de tours : " + j.getNbTours());

		// On ajoute l'affichage du nombre de tours au panel principal
		container.add(tours);

		// On place la fenetre au centre de l'ecran
		this.setLocation(
				(int) (screen.getWidth() / 2 - container.getWidth() / 2),
				(int) (screen.getHeight() / 2 - container.getHeight() / 2));

		// On applique l'arret du programme lorsque l'on ferme la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// On adapte la taille de la fenetre et on l'affiche
		this.pack();
		this.setVisible(true);
	}
}

package graphisme;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author GENY Vanessa
 *
 */
public class Theme extends JFrame {

    Color normale = Color.darkGray, montee = new Color(244, 75, 66), descente = new Color(66, 134, 244),
	    bande = Color.white, fond = new Color(58, 175, 31);
    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    String[] listeCases = { "Normale", "Montee", "Descente" };
    Color[] listeCouleur = { normale, montee, descente, bande, fond };

    /**
     * Constructeur de la fenetre d'edition du theme
     */
    public Theme() {
	
	//On intialise le titre de la fenetre et son container principal
	this.setTitle("Mode Creation");
	JPanel container = new JPanel() {
	    @Override
	    protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		//On cree le fond
		g.setColor(listeCouleur[4]);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		for (int i = 0; i < 3; i++) {
		    
		    //On cree les differents modele de route
		    g.setColor(Color.BLACK);
		    g.drawString(listeCases[i], 30 + 150 * i, this.getHeight() - 115);
		    g.setColor(listeCouleur[i]);
		    g.fillRect((5 + i * 150), this.getHeight() - 105, 100, 100);
		    g.setColor(listeCouleur[3]);
		    g.fillRect((12 + i * 150), this.getHeight() - 60, 85, 10);
		}
	    }
	};
	
	//On cree  les differents boutons pour modifier les couleurs
	JButton bN = new JButton();
	bN.setIcon(new Icon() {

	    @Override
	    public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(listeCouleur[0]);
		g.fillRect(x, y, 100, 100);
	    }

	    @Override
	    public int getIconWidth() {
		return 100;
	    }

	    @Override
	    public int getIconHeight() {
		return 100;
	    }
	});
	container.add(bN);

	JButton bM = new JButton();
	bM.setIcon(new Icon() {

	    @Override
	    public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(listeCouleur[1]);
		g.fillRect(x, y, 100, 100);
	    }

	    @Override
	    public int getIconWidth() {
		return 100;
	    }

	    @Override
	    public int getIconHeight() {
		return 100;
	    }
	});
	container.add(bM);

	JButton bD = new JButton();
	bD.setIcon(new Icon() {

	    @Override
	    public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(listeCouleur[2]);
		g.fillRect(x, y, 100, 100);
	    }

	    @Override
	    public int getIconWidth() {
		return 100;
	    }

	    @Override
	    public int getIconHeight() {
		return 100;
	    }
	});
	container.add(bD);

	JButton bB = new JButton();
	bB.setIcon(new Icon() {

	    @Override
	    public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(listeCouleur[3]);
		g.fillRect(x, y, 100, 100);
	    }

	    @Override
	    public int getIconWidth() {
		return 100;
	    }

	    @Override
	    public int getIconHeight() {
		return 100;
	    }
	});
	container.add(bB);

	JButton bF = new JButton();
	bF.setIcon(new Icon() {

	    @Override
	    public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(listeCouleur[4]);
		g.fillRect(x, y, 100, 100);
	    }

	    @Override
	    public int getIconWidth() {
		return 100;
	    }

	    @Override
	    public int getIconHeight() {
		return 100;
	    }
	});
	container.add(bF);

	//On cree l'action listener
	ActionListener al = new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		//On cree un colorchooser afin de faire changer la couleur
		JColorChooser jcc = new JColorChooser();
		
		//On le fait apparaitre
		JColorChooser.createDialog(Theme.this, "Personnalisation", true, jcc, null, null).setVisible(true);
		
		//En fonction du bouton sur lequel on a clique, une des couleurs se change
		if (e.getSource() == container.getComponent(0)) {
		    listeCouleur[0] = jcc.getColor();
		} else if (e.getSource() == container.getComponent(1)) {
		    listeCouleur[1] = jcc.getColor();
		}

		else if (e.getSource() == container.getComponent(2)) {
		    listeCouleur[2] = jcc.getColor();
		}

		else if (e.getSource() == container.getComponent(3)) {
		    listeCouleur[3] = jcc.getColor();
		}

		else if (e.getSource() == container.getComponent(4)) {
		    listeCouleur[4] = jcc.getColor();
		}

		//On actualise la fenetre
		container.repaint();
	    }
	};
	
	//On ajoute l'action listener a tous les boutons
	bN.addActionListener(al);
	bM.addActionListener(al);
	bD.addActionListener(al);
	bB.addActionListener(al);
	bF.addActionListener(al);

	
	//On cree un bouton de sauvegarde
	JButton save = new JButton("Sauvegarder");
	save.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		Theme.this.sauvegarder();

	    }
	});
	
	
	//On cree un bouton pour revenir au menu
	JButton menu = new JButton("Menu");
	menu.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		Theme.this.dispose();
		new FenetreAccueil();

	    }
	});
	
	//On les ajoute a la fenetre
	container.add(save);
	container.add(menu);
	
	//On definit les tailles, localisations
	container.setPreferredSize(new Dimension(700, 700));
	this.setLocation(screen.width / 2 - container.getPreferredSize().width / 2,
		screen.height / 2 - container.getPreferredSize().height / 2);
	this.setContentPane(container);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.pack();
	this.setVisible(true);
    }

    /**
     * Methode permettant de sauvegarder un theme
     */
    protected void sauvegarder() {
	
	//On fait choisir un nom a l'utilisateur
	String nom = JOptionPane.showInputDialog("Choisissez le nom du theme");
	if (nom.equals("")) {
	    nom = "theme1";
	}
	try {
	    
	    //On cree un une instance afin d'enregistrer les couleurs dans un fichier avec le nom choisi
	    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data/themes/" + nom + ".thm"));
	    
	    //Pour chaque couleur du theme
	    for (Color c : listeCouleur) {
		
		//On l'ecrit dans le fichier
		oos.writeObject(c);
	    }
	    //On ferme le fichier
	    oos.close();
	} catch (FileNotFoundException e) {
	    System.out.println("Erreur lors de la creation du fichier theme");
	} catch (IOException e) {
	    System.out.println("Erreur lors de l'ecriture");
	}

    }
}
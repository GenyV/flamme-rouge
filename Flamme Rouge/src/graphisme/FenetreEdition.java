package graphisme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import application.plateau.Cellule;

/**
 * @author GENY Vanessa
 *
 */
public class FenetreEdition extends JFrame {
    /*
     * Declaration des variables de classe
     */
    //On recupere la dimension de l'ecran
    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    
    //on cree une variable static qui recuperera l'affichage du plateau
    private static PanelPaysage etape;
    
    /**
     * Constructeur creant la fenetre d'edition
     */
    public FenetreEdition() {
	
	//On definit le titre de la fenetre
	this.setTitle("Mode Creation");
	
	//On definit l'action du bouton fermer par la fin du programme
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	//On cree un panel principal
	JPanel container = new JPanel();
	
	//On definit la taille du panel principal
	container.setPreferredSize(new Dimension(800, (int) (screen.height*0.8)));
	
	//On lui affecte un layout manager
	container.setLayout(new BorderLayout());
	
	//On definit container comme panel principal de la fenetre
	this.setContentPane(container);
	
	//On cree un nouveau panel pour l'affichage du plateau
	etape = new PanelPaysage();
	
	//On definit la taille du panel
	etape.setPreferredSize(new Dimension(container.getPreferredSize().width, (int) (screen.height*0.6)));
	
	//on met le plateau au centre de la fenetre
	container.add(etape, BorderLayout.CENTER);
	
	//On cree un nouveau panel pour l'affichage des cases cliquables
	JPanel outils = new JPanel();
	
	//On definit la taille du panel
	outils.setPreferredSize(new Dimension(screen.width-100,  (int) (screen.height*0.2)));
	
	
	//On cree un bouton pour les routes normales
	JButton rNormale = new JButton();
	rNormale.addActionListener(new ActionListener() {
	    
	    //On ajoute la case au plateau
	    @Override
	    public void actionPerformed(ActionEvent e) {
		etape.ajouterCase(new Cellule(Cellule.NORMALE));
	    }
	});
	
	try {
	    //On definit un icone pour le bouton
	    rNormale.setIcon(new ImageIcon(ImageIO.read(new File("data/creation/normale.png"))));
	} catch (IOException e1) {
	    System.out.println("Image route normale non trouvee");
	}

	//On ajoute le bouton au panel
	outils.add(rNormale);
	
	//Meme operation
	JButton rMontee = new JButton();
	rMontee.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		etape.ajouterCase(new Cellule(Cellule.MONTEE));
	    }
	});
	
	try {
	    rMontee.setIcon(new ImageIcon(ImageIO.read(new File("data/creation/montee.png"))));
	} catch (IOException e1) {
	    System.out.println("Image route montee non trouvee");
	}

	outils.add(rMontee);
	
	//Meme operation
	JButton rDescente = new JButton();
	rDescente.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		etape.ajouterCase(new Cellule(Cellule.DESCENTE));
	    }
	});
	
	try {
	    rDescente.setIcon(new ImageIcon(ImageIO.read(new File("data/creation/descente.png"))));
	} catch (IOException e1) {
	    System.out.println("Image route descente non trouvee");
	}

	outils.add(rDescente);
	
	//Meme operation
	JButton rArrivee = new JButton();
	rArrivee.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		etape.ajouterCase(new Cellule(Cellule.ARRIVEE));
	    }
	});
	
	try {
	    rArrivee.setIcon(new ImageIcon(ImageIO.read(new File("data/creation/arrivee.png"))));
	} catch (IOException e1) {
	    System.out.println("Image route arrivee non trouvee");
	}

	outils.add(rArrivee);
	
	//On cree un bouton pour tout supprimer
	JButton suppr = new JButton("Tout effacer");
	suppr.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		etape.toutSupprimer();
		
	    }
	});
	
	//On l'ajoute au panels
	outils.add(suppr);
	
	
	//On ajoute un bouton ne supprimant que la case selectionee
	JButton supprSelect = new JButton("Supprimer la case selectionee");
	supprSelect.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		//on appelle la methode reinitialisant tout le plateau
		etape.supprimerSelection();
	    }
	});
	
	//on ajoute le bouton a la fenetre
	outils.add(supprSelect);
	
	//On cree un bouton permettant la sauvegarde
	JButton sauvegarder = new JButton("Sauvegarder");
	sauvegarder.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		//On recupere le nom du fichier et on le sauvegardant 
		String nom = JOptionPane.showInputDialog("Nom du circuit");
		etape.sauvegarder(nom);
		
	    }
	});
	
	//On ajoute le bouton au panel
	outils.add(sauvegarder);
	
	
	//Creation permettant de revenir au menu
	JButton menu = new JButton("Menu");
	menu.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		//La fenetre se forme et la fenetre d'accueil se lance
		FenetreEdition.this.dispose();
		new FenetreAccueil();
		
	    }
	});
	
	//On rajoute le bouton au panel
	outils.add(menu);
	
	//On ajoute le panel en bas du panel principal
	container.add(outils, BorderLayout.SOUTH);
	
	
	//on adapte la taille de la fenetre et on la rend visible
	this.pack();
	this.setVisible(true);
    }
}
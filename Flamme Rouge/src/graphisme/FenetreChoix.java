package graphisme;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Classe initialisant la fenetre permettant a l'utilisateur de saisir le nombre de joueurs, heritage de JFrame
 */
public class FenetreChoix extends JFrame {

    // Initialisation d'une variable statique pour l'affichage
    private static int NBJ;

    // On recupere la taille de l'ecran
    private Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

    // On cree la variable recuperant le choix de l'utilisateur
    private int choix;

    /**
     * Constructeur de la fenetre choix
     */
    public FenetreChoix() {

	// Creation du panel de base de la fenetre
	JPanel container = new JPanel();

	// Attribution du titre de la fenetre en "Flamme Rouge"
	this.setTitle("Flamme Rouge");

	// On empeche la redimension
	this.setResizable(false);

	// On parametre l'action du bouton fermer
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	// On attribue le panel parent
	this.setContentPane(container);

	// On positionne la fenetre au centres
	this.setLocation((int) (screen.getWidth() / 2 - 150), (int) (screen.getHeight() / 2 - 50));

	// On cree un label
	JLabel t1 = new JLabel("Nombre de Joueurs");

	// On initialise le nombre de joueurs de base � 1
	NBJ = 1;

	// On cree le panel permettant de choisir le nombre de joueurs
	JPanel choix = new JPanel() {
	    @Override
	    protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// On dessine le nombre actuel de joueur
		g.setFont(new Font("Arial Bold", Font.CENTER_BASELINE, 20));
		g.drawString("" + NBJ, (int) this.getPreferredSize().getWidth() / 2, 40);
	    }
	};

	// On attribue une dimension
	choix.setPreferredSize(new Dimension(300, 100));

	// On cree le bouton permettant de reduire le nombre de joueurs
	JButton moins = new JButton();

	// Lorsque l'on clique dessus
	moins.addActionListener(new ActionListener() {

	    @Override

	    public void actionPerformed(ActionEvent e) {

		// Si le nombre de joueurs est strictement superieur a 1, alors on le decremente
		// de 1
		if (NBJ > 1)
		    FenetreChoix.NBJ--;
		// actualisation de la fenetre
		repaint();
	    }
	});

	// On cree le bouton permettant d'augmenter le nombre de joueurs
	JButton plus = new JButton();

	// Lorsque l'on clique dessus
	plus.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		// Si le nombre de joueur est strictement inferieur a 4, alors on l'incremente
		// de 1
		if (NBJ < 4)
		    FenetreChoix.NBJ++;

		// actualisation de la fenetre
		repaint();
	    }
	});

	// Creation du bouton de validation
	JButton ok = new JButton("ALLONS Y");

	// Si on clique dessus
	ok.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		// Cette fenetre se ferme et on ouvre la fenetre permettant le choix du nombre
		// d'IA
		FenetreChoix.this.dispose();
		new FenetreChoixIA(NBJ);

	    }
	});

	//On attribue des icones de fleche aux 2 boutons
	moins.setIcon(new ImageIcon("data/fg.png"));
	plus.setIcon(new ImageIcon("data/fd.png"));
	
	//On ajoute tous les composants de la fenetre au panel principal
	container.add(t1);
	container.add(choix);
		
	//On retire le layout du panel des boutons pour pouvoir placer manuellement les boutons
	choix.setLayout(null);
	//on rajoute les boutons au panel choix
	choix.add(moins);
	choix.add(plus);
	choix.add(ok);
	
	//On place les boutons dans le panel
	moins.setBounds((int) (choix.getPreferredSize().getWidth() * 0.1 - 20), 20, 20, 20);
	plus.setBounds((int) (choix.getPreferredSize().getWidth() * 0.9), 20, 20, 20);
	ok.setBounds((int) (choix.getPreferredSize().getWidth() / 2 - 95 / 2), 60, 95, 30);
	
	//On definit la taille du panel principal
	container.setPreferredSize(new Dimension((int) choix.getPreferredSize().getWidth(), 125));
	
	//On adapte la taille de la fenetre et on l'affiche
	this.pack();
	this.setVisible(true);
    }
}
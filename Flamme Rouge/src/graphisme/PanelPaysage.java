package graphisme;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import application.jeu.Jeu;
import application.pions.Pion;
import application.pions.Sprinteur;
import application.plateau.Cellule;
import application.plateau.Etape;

/**
 * @author GENY Vanessa
 *
 */
public class PanelPaysage extends JPanel {

    /*
     * On cree des variables de classe
     */

    // Jeu en cours
    private Jeu j;

    // Etape a afficher
    private Etape e;

    // Booleen verifiant l'etat du parcours du circuit
    private boolean finCircuit = false;

    // Variables pour le graphisme
    private int caseTaille = 80, espacement = 25, bande = 5, pi = 30, selectX = -1, selectY = -1, precIndex = -1;

    // variable pour la cellule selectionee
    private Cellule select;

    private Color[] listeCouleur = new Color[5];

    /**
     * Constructeur pour le jeu normal
     * 
     * @param jeu
     *            jeu en cours
     * @param etape
     *            etape a modeliser
     */
    public PanelPaysage(Jeu jeu, Etape etape) {
	this.j = jeu;
	this.e = etape;
	this.initialiserCouleur();

    }

    /**
     * Constructeur pour le mode edition
     */
    public PanelPaysage() {

	// On definit les couleurs du panel en fonction du theme choisi
	this.initialiserCouleur();

	// On cree un plateau vierge
	this.e = new Etape();

	/* CONFIGURATION DU DRAG AND DROP */

	// On ajoute un mouse listener
	this.addMouseListener(new MouseListener() {

	    @Override

	    // Si on lache le clique, alors on remet la case selectionee dans le plateau
	    public void mouseReleased(MouseEvent e) {
		PanelPaysage.this.remettreCase(e.getX(), e.getY());
	    }

	    @Override

	    // On selectionne une case en cliquant dessus
	    public void mousePressed(MouseEvent e) {
		PanelPaysage.this.selectionnerCase(e.getX(), e.getY());

	    }

	    @Override
	    public void mouseExited(MouseEvent e) {
	    }

	    @Override
	    public void mouseEntered(MouseEvent e) {
	    }

	    @Override
	    public void mouseClicked(MouseEvent e) {
	    }
	});

	// On ajoute un motion listener pour le "drag"
	this.addMouseMotionListener(new MouseMotionListener() {

	    @Override
	    public void mouseMoved(MouseEvent e) {

	    }

	    @Override
	    // On bouge la case selectionee
	    public void mouseDragged(MouseEvent e) {
		PanelPaysage.this.bougerCase(e.getX(), e.getY());

	    }
	});
    }

    /**
     * @param x
     *            abscisse ou bouger la case
     * @param y
     *            ordonnee ou bouger la case
     */
    public void bougerCase(int x, int y) {

	// On recupere le plateau de l'etape
	// On verifie que la case n'est pas dans le plateau
	if (this.e.getPlateau().contains(this.select))
	    this.e.getPlateau().remove(this.select);

	// On place la case au centre de la souris
	this.selectX = x - this.caseTaille / 2;
	this.selectY = y - this.caseTaille / 2;

	// On recupere l'index de la case dans le plateau en fonction de ses coordonnees
	int index = this.indexCase(x, y);

	/*
	 * precIndex est l'index precedent d'ou se trouver la case, afin de cree un
	 * "espace" la ou se trouve la case que l'on drag sur le plateaa on cree une
	 * case vide a cet index
	 */

	// On supprime l'ancienne case vide se trouvant a l'ancien index de la case si
	// c'est possible
	if (precIndex > -1 && precIndex < this.e.getPlateau().size())
	    this.e.getPlateau().remove(precIndex);

	// On recree une case vide afin de recreer l'espace
	if (index > -1 && index < this.e.getPlateau().size())
	    this.e.getPlateau().add(index, null);

	// On change la valeur de precIndex par l'index actuel
	this.precIndex = index;

	// On actualise la fenetre
	this.repaint();
    }

    /**
     * @param x
     *            abscisse
     * @param y
     *            ordonnee
     * @return l'index des coordonnees dans le plateau
     */
    public int indexCase(int x, int y) {

	// On initialise l'index a -1
	int index = -1;

	// On recupere l'index au niveau de la largeur
	int indexLargeur = x / (this.caseTaille + 5);

	// On recupere l'index au niveau de la hauteur
	int indexHauteur = y / (this.caseTaille + this.espacement);

	// On recupere l'index a partir des 2 index
	/*
	 * indexLargeur + indexHauteur*nbMaxCases par ligne
	 */
	index = indexLargeur + indexHauteur * ((this.getWidth() - 10) / this.caseTaille);

	// On retourne l'index
	return index;
    }

    /**
     * @param x
     *            abscisse du clic
     * @param y
     *            ordonnee du clic
     */
    public void selectionnerCase(int x, int y) {

	int index = -1;

	/*
	 * L'operation ici est differente pour calculer l'index puisqu'entre chaque
	 * cellule se trouve un espacement a prendre en compte
	 */
	// On calcul l'index en largeur de la cellule
	int indexLargeur = x / (this.caseTaille + 5);

	// On calcul l'index en hauteur de la cellule
	int indexHauteur = y / (this.caseTaille + this.espacement);

	// Si l'abscisse se trouve bien dans une case est pas entre deux cases
	if (x >= indexLargeur * (this.caseTaille + 5) && x <= indexLargeur * (this.caseTaille + 5) + this.caseTaille) {

	    // Si l'ordonnee se trouve bien dans une case et non entre deux cases
	    if (y >= indexHauteur * (this.caseTaille + this.espacement)
		    && y <= indexHauteur * (this.caseTaille + this.espacement) + this.caseTaille) {

		// Alors l'index est valide et on l'attribut
		index = indexLargeur + indexHauteur * ((this.getWidth() - 10) / this.caseTaille);
	    }
	}

	// Si l'index est bien valide
	if (index <= this.e.getPlateau().size() && index > -1) {

	    // On selectionne la case a cet index
	    this.select = this.e.getPlateau().get(index);

	    // On attribut les coordonnees pour bouger la case
	    this.selectX = indexLargeur * (this.caseTaille + 5);
	    this.selectY = indexHauteur * (this.caseTaille + this.espacement);
	}

	// On actualise le panel
	this.repaint();
    }

    /**
     * @param x
     *            abscisse de la case
     * @param y
     *            ordonnee de la case
     */
    public void remettreCase(int x, int y) {

	// On recupere l'index de la case
	int index = indexCase(x, y);

	// On supprime la case selectionnee du plateau
	this.e.getPlateau().remove(this.select);

	// Si l'index sort du plateau et qu'il n'y a pas de case arrivee ou que la case
	// est une case arrivee
	if ((index >= this.e.getPlateau().size() && !this.finCircuit) || (this.select.getType() == Cellule.ARRIVEE))

	    // On positionne la case a la fin du plateau
	    this.e.getPlateau().addLast(this.select);

	// Sinon si l'index sort du plateau
	else if (index >= this.e.getPlateau().size()) {

	    // On le place sur l'avant derniere case du plateau
	    this.e.getPlateau().add(this.e.getPlateau().size() - 1, this.select);

	    // Sinon on le place a l'index
	} else
	    this.e.getPlateau().add(index, this.select);

	// Si l'index precedent (c-a-d que la case a ete en drag and drop)
	if (precIndex != -1) {

	    // la case selectionnee devient null
	    this.select = null;

	    // On reinitialise ses coordonnees
	    this.selectX = -1;
	    this.selectY = -1;

	    // Tant qu'il y a des cases invisibles dans le plateau
	    while (this.e.getPlateau().contains(null))

		// On les enleve
		this.e.getPlateau().remove(null);

	    // On reinitialise la valeur de precIndex
	    this.precIndex = -1;
	}

	// On actualise la fenetre
	this.repaint();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    protected void paintComponent(Graphics g) {

	// On appelle la methode parent
	super.paintComponent(g);

	// On dessine le fond vert
	g.setColor(this.listeCouleur[4]);
	g.fillRect(0, 0, this.getWidth(), this.getHeight());

	// On calcul le nombre de cases maximal par ligne
	int caseMaxLargeur = (this.getWidth() - 10) / caseTaille;

	// On cree une variable d'incrementation
	int i = 0;

	// On intialise les coordonnees de la premiere cellule affichee
	int x = 0;
	int y = 0;

	// On cree une variable contenant les images des routes
	BufferedImage bi;

	// Pour chaque cellule du plateau
	for (Cellule c : e.getPlateau()) {

	    // Si le cellule n'est pas selectionnee ou nulls
	    if (c != this.select && c != null) {

		// On calcule son abscisse (Taille de la case * le nombre de cases deja presente
		// sur la ligne, (reste de la division euclidienne)
		x = (5 + caseTaille) * (i % caseMaxLargeur);

		// On calcule son ordonnee (Taille de la case + l'espacement * le nombre de
		// lignes deja presentes (division euclidienne)
		y = (espacement + caseTaille) * (i / caseMaxLargeur);

		// En fonction du type de la cellule, sa couleur de fond sera differente
		switch (c.getType()) {
		case (Cellule.MONTEE):
		    g.setColor(this.listeCouleur[1]);
		    break;

		case (Cellule.DESCENTE):
		    g.setColor(this.listeCouleur[2]);
		    break;

		// Si c'est une cellule arrivee, on cree un damier
		case (Cellule.ARRIVEE):
		    int nbCarreau = 5;
		    int carreau = caseTaille / nbCarreau;
		    for (int j = 0; j < nbCarreau; j++) {
			for (int k = 0; k < nbCarreau; k++) {
			    if (j % 2 == k % 2)
				g.setColor(Color.BLACK);
			    else
				g.setColor(Color.white);
			    g.fillRect(x + k * carreau, y + j * carreau, carreau, carreau);
			}
		    }
		    break;

		// Couleur des routes normales par defaut
		default:
		    g.setColor(this.listeCouleur[0]);
		    break;
		}

		// Si ce n'est pas une cellule arrivee
		if (c.getType() != Cellule.ARRIVEE) {

		    // Alors on cree un carre rempli de la couleur selectionnee
		    g.fillRect(x, y, caseTaille, caseTaille);

		    // On cree une bande blanche au centre
		    g.setColor(this.listeCouleur[3]);
		    g.fillRect(x, y + caseTaille / 2 - bande / 2, caseTaille, bande);
		}

		// Si la cellule est occupee sur au moins une de ses deux cases
		if (c.moitieOccupee() != null) {

		    // Si elle est totalement occupee
		    if (c.isOccupee()) {

			// On recupere le pion a gauche
			Pion pG = c.getGauche().getP();
			String type = "";

			// Si il n'est pas null
			if (pG != null) {

			    // On recupere son type
			    if (pG instanceof Sprinteur)
				type = "Sprinteur";
			    else
				type = "Rouleur";

			    try {
				// On charge son image
				bi = ImageIO.read(new File("data/" + pG.getC() + "/" + type + ".png"));

				// Si l'image n'est pas chargee
				if (bi == null) {

				    // Alors on cree un rond de sa couleurs
				    g.setColor(recupererCouleur(pG.getC()));
				    g.fillOval(x + caseTaille / 2 - pi / 2, y + (caseTaille / 2 - bande / 2 - pi) / 2,
					    pi, pi);
				    // Sinon
				} else {
				    // On recupere la largeur et la hauteur de l'image
				    int w = bi.getWidth(null);
				    int h = bi.getHeight(null);

				    // On calcule le ratio
				    double ratio = w / h;

				    // On recupere la hauteur d'une case
				    pi = caseTaille / 2 - bande / 2 - 5;
				    // On dessine l'image au centre de la case
				    g.drawImage(bi, x + caseTaille / 2 - pi / 2,
					    y + (caseTaille / 2 - bande / 2 - pi) / 2, (int) (pi * ratio), pi, null);
				}
			    } catch (IOException e1) {
				System.out.println("Probleme chargement case gauche (Cellule totalement occupee");
			    }
			}
			// On reinitialise le type
			type = "";

			// On recupere le pion a droite
			Pion pD = c.getDroite().getP();

			// Meme operation que pour le pion gauche
			if (pD != null) {

			    if (pD instanceof Sprinteur)
				type = "Sprinteur";
			    else
				type = "Rouleur";

			    try {
				BufferedImage bi2 = ImageIO.read(new File("data/" + pD.getC() + "/" + type + ".png"));
				if (bi2 == null) {
				    g.setColor(recupererCouleur(pD.getC()));
				    g.fillOval(x + caseTaille / 2 - pi / 2,
					    y + (caseTaille / 2 + bande / 2) + (caseTaille / 2 - bande / 2 - pi) / 2,
					    pi, pi);
				} else {
				    int w = bi2.getWidth(null);
				    int h = bi2.getHeight(null);
				    double ratio = w / h;
				    pi = caseTaille / 2 - bande / 2 - 5;
				    g.drawImage(bi2, x + caseTaille / 2 - pi / 2,
					    y + (caseTaille / 2 + bande / 2) + (caseTaille / 2 - bande / 2 - pi) / 2,
					    (int) (pi * ratio), pi, null);
				}
			    } catch (IOException e1) {
				System.out.println("Probleme chargement case droite (Cellule totalement occupee");
			    }
			}

			// Sinon si une des deux cases est occupee
		    } else {
			int cote = 0;
			String type = "";

			// Si le pion de la case occupee existe bien
			if (c.moitieOccupee().getP() != null) {

			    // On recupere sa couleur
			    String couleur = c.moitieOccupee().getP().getC();
			    Color col = recupererCouleur(couleur);
			    g.setColor(col);

			    // Si c'est la case droite alors on intialise l'attribut cote a 1
			    if (c.moitieOccupee() == c.getDroite()) {
				cote = 1;
			    }
			    try {
				// On recupere le type du pion
				if (c.moitieOccupee().getP() instanceof Sprinteur)
				    type = "Sprinteur";
				else
				    type = "Rouleur";

				// On lit l'image du pion
				bi = ImageIO.read(
					new File("data/" + c.moitieOccupee().getP().getC() + "/" + type + ".png"));

				// Si on arrive pas a la lire, on cree un pion a la place
				if (bi == null)
				    g.fillOval(x + caseTaille / 2 - pi / 2, y + cote * (caseTaille / 2 + bande / 2)
					    + (caseTaille / 2 - bande / 2 - pi) / 2, pi, pi);
				else {
				    // On le place au centre de sa case
				    int w = bi.getWidth(null);
				    int h = bi.getHeight(null);
				    double ratio = w / h;
				    pi = caseTaille / 2 - bande / 2 - 5;
				    g.drawImage(bi, x + caseTaille / 2 - pi / 2,
					    y + cote * (caseTaille / 2 + bande / 2)
						    + (caseTaille / 2 - bande / 2 - pi) / 2,
					    (int) (pi * ratio), pi, null);
				}
			    } catch (IOException e2) {
				System.out.println("Erreur image pions");
			    }
			}
		    }
		}
	    }
	    i++;
	}

	// On s'occupe de dessiner la case selectionnee
	// Meme operation que pour les cases normales
	if (this.select != null) {
	    switch (this.select.getType()) {
	    case (Cellule.MONTEE):
		g.setColor(this.listeCouleur[1]);
		break;

	    case (Cellule.DESCENTE):
		g.setColor(this.listeCouleur[2]);
		break;

	    case (Cellule.ARRIVEE):
		int nbCarreau = 5;
		int carreau = caseTaille / nbCarreau;
		for (int j = 0; j < nbCarreau; j++) {
		    for (int k = 0; k < nbCarreau; k++) {
			if (j % 2 == k % 2)
			    g.setColor(Color.BLACK);
			else
			    g.setColor(Color.white);

			// On la dessine a ses coordonnees
			g.fillRect(this.selectX + k * carreau, this.selectY + j * carreau, carreau, carreau);
		    }
		}
		break;
	    default:
		g.setColor(this.listeCouleur[0]);
		break;
	    }
	    if (this.select.getType() != Cellule.ARRIVEE) {
		g.fillRect(this.selectX, this.selectY, caseTaille, caseTaille);
		g.setColor(this.listeCouleur[3]);
		g.fillRect(this.selectX, this.selectY + caseTaille / 2 - bande / 2, caseTaille, bande);
	    }
	    g.setColor(Color.blue);
	    for (int j = 0; j < 5; j++) {
		g.drawRect(this.selectX + j, this.selectY + j, caseTaille - 1 - 2 * j, caseTaille - 1 - 2 * j);
	    }
	}

    }

    /**
     * Methode permettant de supprimer la cellule selectionnee
     */
    public void supprimerSelection() {

	// Si il y a bien une cellule selectionnee
	if (this.select != null) {

	    // Si c'est une case arrivee alors l'attribut finCircuit redevient faux
	    if (this.select.getType() == Cellule.ARRIVEE)
		this.finCircuit = false;
	    // On enleve la case du plateau
	    this.e.getPlateau().remove(this.select);

	    // On la remet a null
	    this.select = null;

	    // On actualise la fenetre
	    this.repaint();
	}

    }

    /**
     * Methode renvoyant une couleur en fonction de sa lettre
     * 
     * @param couleur
     * @return la couleur en fonction du parametre
     */
    private Color recupererCouleur(String couleur) {
	Color col = null;
	if (couleur.equals("R"))
	    col = Color.red;
	else if (couleur.equals("V"))
	    col = Color.green;
	else if (couleur.equals("B"))
	    col = Color.BLUE;
	else
	    col = Color.BLACK;
	return col;
    }

    /**
     * Methode permettant d'actualiser le panel Utile lors des deplacements des
     * pions afin de voir l'animation
     */
    public void modifier() {
	try {
	    // On attend 250 ms et on actualise la fenetre
	    Thread.sleep(250);
	} catch (InterruptedException e) {
	    System.out.println("Erreur pendant le deplacement (thread.sleep)");
	}
	this.repaint();
    }

    /**
     * @param c
     *            Cellule a ajouters
     */
    public void ajouterCase(Cellule c) {

	// Sinon on a pas mit une case arrivee alors on ajoute la cellule au plateau
	if (!this.finCircuit)
	    this.e.getPlateau().add(c);

	// Si on ajoute une case arrivee, alors l'attribut finCircuit devient vrai
	if (c.getType() == Cellule.ARRIVEE)
	    this.finCircuit = true;

	// On actualise la fenetre
	this.repaint();
    }

    /**
     * Methode permettant de sauvegarder un circuit
     * 
     * @param nom
     *            nom du circuit a sauvegarder
     */
    public void sauvegarder(String nom) {
	try {

	    // On cree un nouveau buffered Writer avec en parametre un fichier avec le nom
	    // choisi
	    BufferedWriter bw = new BufferedWriter(new FileWriter("data/circuits/" + nom.toLowerCase() + ".circuit"));

	    // On initialise le string symbolisant le circuit a vide
	    String cir = "";

	    // Pour chaque cellule, on ajoute au string son entier de type
	    for (Cellule c : e.getPlateau()) {
		cir += c.getType();
	    }

	    // On ecrit la ligne puis on ferme le fichier
	    bw.write(cir);
	    bw.close();
	} catch (IOException e) {
	    System.out.println("Probleme d'E/S lors de la sauvegarde du circuit");
	}

    }

    /**
     * Methode permettant de reinitialiser le plateau Utile en mode creation
     */
    public void toutSupprimer() {

	// On reinitialise le plateau
	e = new Etape();

	// On reinitialise la selection
	this.select = null;

	// On met a jour l'affichage
	this.repaint();
    }

    /**
     * Methode permettant d'initialiser les couleurs en fonction du theme choisi
     */
    private void initialiserCouleur() {
	try {

	    // On cree un buffered reader qui lira le fichier de configuration
	    BufferedReader br = new BufferedReader(new FileReader("data/config.txt"));

	    // On initialise le lecteur du fichier du theme
	    ObjectInputStream ois = null;

	    // On lit la premiere ligne du fichier de configuration
	    String ligne = br.readLine();

	    // On initialise une variable temporaire
	    String temp = "";

	    // Boolean pour l'etat de la recherche
	    boolean ok = false;

	    // Tant qu'on est pas a la fin du fichier
	    while (ligne != null && !ok) {

		// Pour chaque caractere de la ligne
		for (int i = 0; i < ligne.length(); i++) {
		    char c = ligne.charAt(i);

		    // Si c'est un egal
		    if (c == '=') {

			// alors on verifie que c'est bien sur la ligne theme
			if (temp.equals("theme"))
			    temp = "";
			// sinon on arrete le parcours de la ligne
			else
			    break;

			// Si c'est un ; c-a-d une fin de ligne
		    } else if (c == ';') {
			try {

			    // Alors on initialise le lecteur sur le theme du fichier de config
			    ois = new ObjectInputStream(new FileInputStream("data/themes/" + temp + ".thm"));
			} catch (FileNotFoundException e) {

			    // Si le theme n'existe pas, on passe par le theme par defaut
			    ois = new ObjectInputStream(new FileInputStream("data/themes/defaut.thm"));
			}

			// On met ok a true pour arreter la recherche
			ok = true;

			// On arrete la boucle for
			break;

			// Si ce n'est ni un = ou un ; on ajoute le caractere a la variable temporaire
		    } else {
			temp += c;
		    }

		}

		// On incremente la ligne
		ligne = br.readLine();
	    }
	    
	    //Si la ligne n'etait pas presente alors on passe par le theme par defaut
	    if (ois == null)
		ois = new ObjectInputStream(new FileInputStream("data/themes/defaut.thm"));
	    
	    //Variable d'iteration mise a 0
	    int i = 0;
	    
	    //Tant qu'on a pas depasse 4, cad le nombre de couleurs a lire
	    while (i < 5) {
		try {
		    
		    //On initialise la liste couleur avec les objets
		    listeCouleur[i] = (Color) ois.readObject();
		    
		    //On incremente
		    i++;
		} catch (ClassNotFoundException e) {
		    System.out.println("Objet corroumpu");
		}
	    }
	    
	    //On ferme les deux fichiers
	    br.close();
	    ois.close();
	} catch (FileNotFoundException e) {
	    System.out.println("Fichier de configuration inexistant ou introuvable");
	} catch (IOException e) {
	    System.out.println("Probleme lors de la lecture du fichier");
	}
    }

}
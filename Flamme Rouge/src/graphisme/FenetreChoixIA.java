package graphisme;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import application.jeu.Jeu;

public class FenetreChoixIA extends JFrame {
    private Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    private static int NBJ, NBIA;

    public FenetreChoixIA(int nBJ) {

	NBJ = nBJ;
	NBIA = 0;
	JPanel container = new JPanel();

	// Attribution du titre de la fenetre en "Flamme Rouge"
	this.setTitle("Flamme Rouge");

	// On empeche la redimension
	this.setResizable(false);

	// On parametre l'action du bouton fermer
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	// On attribue le panel parent
	this.setContentPane(container);

	// On positionne la fenetre au centres
	this.setLocation((int) (screen.getWidth() / 2 - 150), (int) (screen.getHeight() / 2 - 50));

	// On cree un label
	JLabel t1 = new JLabel("Nombre d'IA");

	// On cree le panel permettant de choisir le nombre d'IA
	JPanel choix = new JPanel() {
	    @Override
	    protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("Arial Bold", Font.CENTER_BASELINE, 20));
		// On dessine le nombre d'IA actuel
		g.drawString("" + NBIA, (int) this.getPreferredSize().getWidth() / 2, 40);
	    }
	};

	// On definit la taille du panel
	choix.setPreferredSize(new Dimension(300, 100));

	// On cree le bouton permettant de reduire le nombre d'IA
	JButton moins = new JButton();

	// Lorsque l'on clique dessus
	moins.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		// Si le nombre d'IA est strictement superieur a 0, on le decremente de 1
		if (NBIA > 0)
		    FenetreChoixIA.NBIA--;
		repaint();
	    }
	});

	// On cree le bouton permettant d'augmenter le nombre d'IA
	JButton plus = new JButton();

	// Lorsque l'on clique dessus
	plus.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		// Si le nombre de joueurs et d'IA est strictement inferieur a 4, on
		// l'incremente de 1
		if (NBJ + NBIA < 4)
		    FenetreChoixIA.NBIA++;
		repaint();
	    }
	});

	// Creation du bouton de validation
	JButton ok = new JButton("ALLONS Y");

	// Si on clique dessus
	ok.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		// Cette fenetre se ferme est on cree un nouveau jeu
		FenetreChoixIA.this.dispose();
		new Jeu(NBJ, NBIA);

	    }
	});

	// On attribue des icones de fleche aux 2 boutons
	moins.setIcon(new ImageIcon("data/fg.png"));
	plus.setIcon(new ImageIcon("data/fd.png"));

	// On ajoute tous les composants de la fenetre au panel principal
	container.add(t1);
	container.add(choix);

	// On retire le layout du panel des boutons pour pouvoir placer manuellement les
	// boutons
	choix.setLayout(null);
	// on rajoute les boutons au panel choix
	choix.add(moins);
	choix.add(plus);
	choix.add(ok);

	// On place les boutons dans le panel
	moins.setBounds((int) (choix.getPreferredSize().getWidth() * 0.1 - 20), 20, 20, 20);
	plus.setBounds((int) (choix.getPreferredSize().getWidth() * 0.9), 20, 20, 20);
	ok.setBounds((int) (choix.getPreferredSize().getWidth() / 2 - 95 / 2), 60, 95, 30);

	// On definit la taille du panel principal
	container.setPreferredSize(new Dimension((int) choix.getPreferredSize().getWidth(), 125));

	// On adapte la taille de la fenetre et on l'affiche
	this.pack();
	this.setVisible(true);
    }

}
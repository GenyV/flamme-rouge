package graphisme;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FenetreConfigurer extends JFrame {

    JComboBox<String> themes;
    int derap = 0;

    /**
     * Constructeur de la fenetre de configuration
     */
    public FenetreConfigurer() {

	// On recupere les donnees de la configuration actuelle
	FenetreConfigurer.recupererConfig(this.derap);

	// On cree une dimension pour chaque ligne des options
	Dimension dimOptions = new Dimension(300, 30);

	// On attribue un titre a la fenetre
	this.setTitle("Mode Configuration");

	// On cree le panel principal
	JPanel container = new JPanel();

	// On cree le sous panel qui contiendra toutes les options
	JPanel options = new JPanel();

	// On cree le panel pour l'option theme
	JPanel optionTheme = new JPanel();

	// On cree le panel pour l'option derapage
	JPanel optionDerapage = new JPanel();

	// On definit container comme panel principal de la fenetre
	this.setContentPane(container);

	// On recupere tous les themes existants
	String[] lTheme = new File("data/themes").list();

	// Pour chaque theme, on supprime l'extension a la fin du nom
	for (int i = 0; i < lTheme.length; i++) {
	    lTheme[i] = lTheme[i].substring(0, lTheme[i].length() - 4);
	}

	// On definit la JComboBox avec la liste des themes
	themes = new JComboBox<>(lTheme);

	// On rajoute le panel options au panel principal
	container.add(options);

	// On definit les differentes tailles
	container.setPreferredSize(new Dimension(300, 300));
	options.setPreferredSize(new Dimension(300, 250));
	options.add(optionTheme);

	// On cree les differents composants
	optionTheme.add(new JLabel("Theme : "));
	optionTheme.add(themes);
	optionTheme.setPreferredSize(dimOptions);
	JLabel jlDerap = new JLabel("Derapage : ");
	jlDerap.setToolTipText("En descente, vous avez une chance de tomber, donc de ne pas avancer au tour suivant");
	optionDerapage.add(jlDerap);
	JCheckBox JCBder = new JCheckBox(new Action() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		if (FenetreConfigurer.this.derap == 0)
		    FenetreConfigurer.this.derap = 1;
		else
		    FenetreConfigurer.this.derap = 0;
	    }

	    @Override
	    public void setEnabled(boolean b) {
	    }

	    @Override
	    public void removePropertyChangeListener(PropertyChangeListener listener) {
	    }

	    @Override
	    public void putValue(String key, Object value) {
	    }

	    @Override
	    public boolean isEnabled() {
		return true;
	    }

	    @Override
	    public Object getValue(String key) {
		return null;
	    }

	    @Override
	    public void addPropertyChangeListener(PropertyChangeListener listener) {
	    }
	});
	if (this.derap == 1)
	    JCBder.setSelected(true);
	optionDerapage.add(JCBder);
	options.add(optionDerapage);
	JButton apply = new JButton("Appliquer");
	apply.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		FenetreConfigurer.this.sauvegarder();
	    }
	});
	container.add(apply);
	JButton menu = new JButton("Menu");
	menu.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		FenetreConfigurer.this.dispose();
		new FenetreAccueil();
	    }
	});
	container.add(menu);

	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.pack();
	this.setVisible(true);
    }

    /**
     * Methode permettant de sauvegarder les configurations
     */
    protected void sauvegarder() {
	try {

	    // On ecrase le fichier de config
	    BufferedWriter bw = new BufferedWriter(new FileWriter("data/config.txt"));

	    // On ecrit le theme choisi et l'option choisi pour les derapages
	    bw.write("theme=" + themes.getSelectedItem() + ";");
	    bw.newLine();
	    bw.write("derapage=" + this.derap + ";");

	    // On ferme le fichier
	    bw.close();
	} catch (IOException e1) {
	    System.out.println("Probleme lors de l'ecriture du fichier config");
	}
    }

    public static void recupererConfig(int paramDerapage) {
	try {
	    
	    //On recupere le fichier de config
	    BufferedReader br = new BufferedReader(new FileReader("data/config.txt"));
	    
	    //On lit la premiere ligne et on intialise une variable temporaire
	    String ligne = br.readLine();
	    String temp = "";
	    
	    //On met la valeur lu a 0
	    int val = 0;
	    
	    //variable permettant de connaitre l'etat de la lecture
	    int prochain = 0;
	    
	    //Tant qu'on est pas a la fin du fichier
	    while (ligne != null) {
		
		//Pour chaque caractere de la ligne actuelle
		for (int i = 0; i < ligne.length(); i++) {
		    char c = ligne.charAt(i);
		    
		    //Si on lit le symbole egal
		    if (c == '=') {
			
			//Si on est pas sur la ligne theme
			if (!temp.equals("theme"))
			    
			    //alors on met la variable d'etat a 1 pour signifier que l'on va lire une valeur
			    prochain = 1;
		    } else if (prochain == 1) {
			//Si on est sur le caractere de la valeur alors on recupere sa valeur numerique
			val = Character.getNumericValue(c);
			
			//On remet prochain a 0
			prochain = 0;
		    } 
		    //Si on lit un ; symbole de fin de ligne, on break la boucle
		    else if (c == ';') {
			break;
		    } 
		    //Sinon on ajoute le caractere a la variable temporaire
		    else {
			temp += c;
		    }
		}
		
		//Si on etait sur la ligne derapage alors le parametre derapage prend la valeur donnee dans le fichier
		if (temp.equals("derapage"))
		    paramDerapage = val;
		
		//On reinitialise la variable temporaire
		temp = "";
		
		//On lit la ligne suivante
		ligne = br.readLine();
	    }
	    
	    //On ferme le fichier
	    br.close();
	} catch (IOException e1) {
	    System.out.println("probleme lors de la lecture du fichier");
	}
    }
}
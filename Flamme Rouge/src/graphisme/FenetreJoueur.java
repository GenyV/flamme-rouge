package graphisme;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import application.cartes.Carte;
import application.cartes.CarteRouleur;
import application.cartes.CarteSprinteur;
import application.cartes.Paquet;
import application.jeu.Jeu;
import application.jeu.Joueur;

/**
 * @author GENY Vanessa
 *
 */
public class FenetreJoueur extends JFrame {

	// On definit un attribut joueur
	Joueur player;

	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * @param j
	 *            joueur proprietaire de la fenetre
	 */
	public FenetreJoueur(Joueur j) {

		// On attribut le joueur en parametre a l'attribut de classe
		this.player = j;

		// On arrete le programme a la fermeture de la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// On cree le panel principal
		JPanel container = new JPanel();

		// On definit sa dimension
		container.setPreferredSize(new Dimension((int) (screen.width * 0.35),
				320));

		// On le definit comme container principal de la fenetre
		this.setContentPane(container);

		// On cree le panel ou l'on va afficher les cartes
		JPanel j2 = new JPanel();

		// On lui definit sa tailles
		j2.setPreferredSize(new Dimension((int) (screen.width * 0.35), 250));

		// On cree un label affichant le numero du joueur
		JLabel jl = new JLabel("Joueur n�" + (j.getNum() + 1) + " "
				+ j.getNom());

		// on ajoute le panel et le label
		container.add(jl);
		container.add(j2);

		int x = 0, y = 0;

		// On fait des test pour definir la position des fenetres en fonction du
		// numero des joueurs
		switch (j.getNum()) {
		case (1):
			x = 0;
			y = 0;
			break;
		case (2):
			x = (int) (screen.width - screen.width * 0.35);
			y = 0;
			break;
		case (3):
			x = 0;
			y = screen.height - 320;
			break;
		case (4):
			x = (int) (screen.width - screen.width * 0.35);
			y = screen.height - 320;
			break;
		}

		// On definit la localisation de la fenetre
		this.setLocation(x, y);

		// On adapte la taille de la fenetre et on l'affiche
		this.pack();
		this.setVisible(true);
	}

	/**
	 * @param i
	 *            index de la carte a afficher en graphique
	 * @param defausse
	 *            defausse utilisee pour les cartes
	 */
	public void AjouterMain(int i, Paquet defausse) {

		// On recupere le panel ou les cartes sont affichees
		JPanel jp2 = (JPanel) this.getContentPane().getComponent(1);

		// On le rend visible
		jp2.setVisible(true);

		// On recupere la carte
		Carte carte = player.getMain().get(i);

		// On definit une variable recuperant le type de la carte
		String type = "";

		// Tests pour recuperer le type
		if (carte instanceof CarteSprinteur) {
			type = "Sprinteur";

		} else if (carte instanceof CarteRouleur)
			type = "Rouleur";
		else {
			type = "Fatigue";
			if (defausse == player.getDS())
				type += "S";
			else
				type += "R";
		}

		// Si la carte n'est pas null
		if (carte != null) {

			// On cree un bouton pour la carte
			JButton c = new JButton();

			// On affiche l'image de la carte
			c.setIcon(new ImageIcon("data/" + player.getCouleur() + "/" + type
					+ carte.getValeur() + ".png"));

			// On definit la fonction du bouton
			c.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					// Si le joueur appuie sur le bouton, alors on lui fait
					// choisir la carte
					player.choisirCarte(i, defausse);

					// On recupere le jeu
					Jeu j = player.getJ();
					if (defausse == player.getDR())

						// On se synchronise avec la phase 1
						synchronized (j.pioche) {
							j.p1fini1 = true;
							j.pioche.notifyAll();
						}

				}
			});

			// On ajoute la carte
			jp2.add(c);

			// On actualise le panel
			jp2.revalidate();
			jp2.repaint();
		}
	}

	/**
	 * methode pour arreter
	 */
	public void detruireMain() {

		// On recupere le panel des cartes
		JPanel jp2 = (JPanel) this.getContentPane().getComponent(1);

		// On efface tous les elements du panel
		jp2.removeAll();

		// On actualise et cache le panel
		jp2.revalidate();
		jp2.setVisible(false);
	}
}
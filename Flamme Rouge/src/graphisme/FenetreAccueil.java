package graphisme;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author GENY Vanessa Classe creant la fenetre d'accueil heritant de la classe
 *         JFrame
 */
public class FenetreAccueil extends JFrame {

    /**
     * Constructeur de la fenetre
     */
    public FenetreAccueil() {

	// On lui attribue le titre Flamme Rouge
	this.setTitle("Flamme Rouge");

	// On cree un JPanem afin d'afficher l'image
	JPanel jp = new JPanel() {
	    @Override
	    protected void paintComponent(Graphics g) {
		try {

		    // Recuperation de l'image dans un buffer
		    BufferedImage img = ImageIO.read(new File("data/accueil.jpg"));
		    g.drawImage(img, 0, 0, null);
		} catch (FileNotFoundException e) {
		    System.out.println("Image d'accueil non trouvee");
		} catch (IOException e2) {
		    System.out.println("Probleme d'E/S, image d'accueil");
		}

	    };
	};

	// Creation du bouton Jouer
	JButton jouer = new JButton("Jouer");

	// Lors du clique sur le bouton
	jouer.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		// On ferme la fenetre et on lance le jeu
		FenetreAccueil.this.dispose();
		new FenetreChoix();
	    }
	});

	// Creation du bouton Edition
	JButton edition = new JButton("Edition");

	// Lors du clique sur le bouton
	edition.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		// On ferme la fenetre et on lance le mode edition
		FenetreAccueil.this.dispose();
		String[] lChoix = { "Plateau", "Theme" };
		int choix = JOptionPane.showOptionDialog(null, "Que souhaitez vous editer", "Mode Creation",
			JOptionPane.OK_CANCEL_OPTION, JOptionPane.YES_NO_CANCEL_OPTION, null, lChoix, lChoix[0]);
		if (choix == 0)
		    new FenetreEdition();
		else
		    new Theme();
	    }
	});

	// Creation du bouton Configurer
	JButton config = new JButton("Configurer");

	// Lors du clique sur le bouton
	config.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		// On ferme la fenetre et on lance le mode edition
		FenetreAccueil.this.dispose();
		new FenetreConfigurer();
	    }
	});

	// On attribut une taille au panel qui sera la taille de la fenetre
	jp.setPreferredSize(new Dimension(562, 567));

	// On ajoute le panel a la fenetre
	this.setContentPane(jp);

	// On ajoute le bouton jouer a la fenetre
	this.add(jouer);

	// On ajoute le bouton edition a la fenetre
	this.add(edition);

	// On ajoute le bouton config a la fenetre
	this.add(config);

	// On empeche la redimension
	this.setResizable(false);

	// On parametre la fonction du bouton fermer
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	// On demande a la fenetre d'adapter sa taille en fonction des panels
	this.pack();

	// On met le layout de la fenetre a null afin de positioner manuellement le
	// bouton
	this.setLayout(null);

	// Positionnement manuel du bouton jouer
	jouer.setBounds(this.getWidth() / 2 - 35, (int) (this.getHeight() * 0.6), 70, 30);

	// Positionnement manuel du bouton edition
	edition.setBounds(this.getWidth() / 2 - 45, (int) (this.getHeight() * 0.6 + 50), 90, 30);
	// Positionnement manuel du bouton config
	config.setBounds(this.getWidth() / 2 - 50, (int) (this.getHeight() * 0.6 + 100), 100, 30);

	// On rend la fenetre visible
	this.setVisible(true);
    }
}
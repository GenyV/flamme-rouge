package graphisme;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import application.jeu.Jeu;
import application.plateau.Etape;

/**
 * @author GENY Vanessa
 *
 */
public class FenetreEtape extends JFrame {
	
    
    	private Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
	/**
	 * @param j jeu actuel
	 * @param e etape actuel
	 */
	public FenetreEtape(Jeu j, Etape e) {
	    
	    	//On definit l'action du bouton fermer par la fin du programme
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//On cree un nouveau panel pour afficher le plateau
		PanelPaysage paysage = new PanelPaysage(j,e);
		
		//On affecte ce panel en tant que panel principal de la fenetre
		this.setContentPane(paysage);
		
		//On definit sa taille
		paysage.setPreferredSize(new Dimension(600,600));
		
		//On le place au centre de l'ecran
		this.setLocation((int)(screen.getWidth()/2-300),(int) (screen.getHeight()/2-300));
		
		//On adapte la taille de la fenetre et on l'affiche
		this.pack();
		this.setVisible(true);
	}
	
}
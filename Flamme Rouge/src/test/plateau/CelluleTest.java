package test.plateau;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.pions.Pion;
import application.plateau.Case;
import application.plateau.Cellule;

public class CelluleTest {

    @Test
    public void testGetMaxAvancee() {
	Cellule c1 = new Cellule(Cellule.NORMALE);
	Cellule c2 = new Cellule(Cellule.MONTEE);
	Cellule c3 = new Cellule(Cellule.DESCENTE);
	Cellule c4 = new Cellule(Cellule.ARRIVEE);
	
	assertEquals("L'avancee max d'une cellule normale devrait etre 9",9,c1.getMaxAvancee());
	assertEquals("L'avancee max d'une cellule montee devrait etre 5",5,c2.getMaxAvancee());
	assertEquals("L'avancee max d'une cellule descente devrait etre 9",9,c3.getMaxAvancee());
	assertEquals("L'avancee max d'une cellule arrivee devrait etre 0",0,c4.getMaxAvancee());
    }

    @Test
    public void testGetMinAvancee() {
	Cellule c1 = new Cellule(Cellule.NORMALE);
	Cellule c2 = new Cellule(Cellule.MONTEE);
	Cellule c3 = new Cellule(Cellule.DESCENTE);
	Cellule c4 = new Cellule(Cellule.ARRIVEE);
	
	assertEquals("L'avancee min d'une cellule normale devrait etre 0",0,c1.getMinAvancee());
	assertEquals("L'avancee min d'une cellule montee devrait etre 0",0,c2.getMinAvancee());
	assertEquals("L'avancee min d'une cellule descente devrait etre 5",5,c3.getMinAvancee());
	assertEquals("L'avancee min d'une cellule arrivee devrait etre 0",0,c4.getMinAvancee());
    }

    @Test
    public void testMoitieOccuper() {
	Cellule c1 = new Cellule(Cellule.NORMALE);
	c1.occuper(new Pion(null,null,null) {
	    
	    @Override
	    public String typePion() {
		// TODO Auto-generated method stub
		return null;
	    }
	});
	
	Case c = c1.moitieOccupee();
	
	assertEquals("La case occupee devrait etre celle de droite", c1.getDroite(),c);
    }
    
    @Test
    //Un pion null
    public void testMoitieOccupe2() {
	Cellule c1 = new Cellule(Cellule.NORMALE);
	c1.occuper(null);
	
	Case c = c1.moitieOccupee();
	
	assertEquals("La case occupee devrait etre aucune", null,c);
    }

}

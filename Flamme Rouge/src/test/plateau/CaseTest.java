package test.plateau;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import application.pions.Pion;
import application.plateau.Case;

public class CaseTest {

    @Test
    //Lorsque le pion existe
    public void testOccuper() {
	Case c = new Case();
	Pion p = new Pion(null,null, null) { 
	    @Override
	    public String typePion() {
		return null;
	    }
	};
	
	c.occuper(p);
	assertEquals("La case devrait etre occupee par le pion p" , p, c.getP());
	assertTrue("La case devrait etre occupee", c.isOccupee());
    }
    
    @Test
    //Lorsque le pion n'existe pas
    public void testOccuper2() {
	Case c = new Case();
	Pion p = null;
	c.occuper(p);
	assertEquals("La case ne devrait pas avoir de pion" , p, c.getP());
	assertFalse("La case devrait etre libre", c.isOccupee());
    }

    @Test
    public void testLiberer() {
	Case c = new Case();
	Pion p = new Pion(null,null, null) { 
	    @Override
	    public String typePion() {
		return null;
	    }
	};
	c.occuper(p);
	
	c.liberer();
	
	assertEquals("La case ne devrait pas avoir de pion" , null, c.getP());
	assertFalse("La case ne devrait pas etre occupee", c.isOccupee());
    }

}

package test.jeu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.cartes.Carte;
import application.cartes.CarteFatigue;
import application.cartes.Paquet;
import application.jeu.Jeu;

public class JeuTest {

	@Test
	//Le classement ne doit pas bouger
	public void testClassement1() {
		Jeu j = new Jeu(0,1);
		j.classement();
		
		assertEquals("Le joueur numero 1 devrait etre le premier de la liste", j.getTabj().get(0).getNum(), 1);
	}
	
	@Test
	//Le classement doit changer
	public void testClassement2() {
		Jeu j = new Jeu(0,4);
		j.classement();
		
		assertEquals("Le joueur numero 4 devrait etre le premier de la liste", j.getTabj().get(0).getNum(), 4);
		assertEquals("Le joueur numero 3 devrait etre le deuxieme de la liste", j.getTabj().get(1).getNum(), 3);
		assertEquals("Le joueur numero 2 devrait etre le troisieme de la liste", j.getTabj().get(2).getNum(), 2);
		assertEquals("Le joueur numero 1 devrait etre le quatrieme de la liste", j.getTabj().get(3).getNum(), 1);
	}
	
	@Test
	//Classement sur un tableau deja classe
	public void testClassement3() {
		Jeu j = new Jeu(0,4);
		j.classement();
		j.classement();
		
		assertEquals("Le joueur numero 4 devrait etre le premier de la liste", j.getTabj().get(0).getNum(), 4);
		assertEquals("Le joueur numero 3 devrait etre le deuxieme de la liste", j.getTabj().get(1).getNum(), 3);
		assertEquals("Le joueur numero 2 devrait etre le troisieme de la liste", j.getTabj().get(2).getNum(), 2);
		assertEquals("Le joueur numero 1 devrait etre le quatrieme de la liste", j.getTabj().get(3).getNum(), 1);
	}

	@Test
	
	//Ajout d'une carte fatigue dans le paquet du sprinteur
	public void testAppliquerFatigue() {
		Jeu j=new Jeu(1,0);
		j.appliquerFatigue(j.getTabj().get(0).getSpr());
		
		int nbCartesFatigueS=0;
		int nbCartesFatigueR=0;
		Paquet p1 = j.getTabj().get(0).getPS();
		Paquet p2 = j.getTabj().get(0).getPR();
		for (int i=0; i<p1.taille();i++){
			if (p1.get(i) instanceof CarteFatigue) nbCartesFatigueS++;
		}
		for (int i=0; i<p2.taille();i++){
			if (p2.get(i) instanceof CarteFatigue) nbCartesFatigueR++;
		}
		
		assertEquals("Il ne devrait avoir qu'une seule carte fatigue dans le paquet sprinteur", 1, nbCartesFatigueS);
		assertEquals("Il ne devrait pas avoir de carte fatigue dans le paquet rouleur", 0, nbCartesFatigueR);
		
		
	}

	@Test
	//Test lorsque le deplacement est possible
	public void testBougerPion1() {
		Jeu j = new Jeu (1,0);
		int index=j.getPlateau().getPlateau().indexOf(j.getTabj().get(0).getSpr().getPosition());
		j.bougerPion(j.getTabj().get(0).getSpr(), 3);
		
		assertEquals("Le pion devrait etre 3 cases plus loin", index+3,j.getPlateau().getPlateau().indexOf(j.getTabj().get(0).getSpr().getPosition()));
	}
	
	@Test
	//Test lorsque le deplacement n'est pas possible
	public void testBougerPion2() {
		Jeu j = new Jeu (4,0);
		int index=j.getPlateau().getPlateau().indexOf(j.getTabj().get(0).getSpr().getPosition());
		j.bougerPion(j.getTabj().get(0).getSpr(), 3);
		
		assertEquals("Le pion devrait etre 3 cases plus loin", index,j.getPlateau().getPlateau().indexOf(j.getTabj().get(0).getSpr().getPosition()));
	}
}

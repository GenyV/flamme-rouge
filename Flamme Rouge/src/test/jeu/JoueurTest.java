package test.jeu;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.cartes.Carte;
import application.jeu.Joueur;

public class JoueurTest {

	@Test
	public void testJoueur() {
		Joueur j = new Joueur(0, "a", "R", null);
		
		assertEquals("Le nom du joueur devrait etre a", "a", j.getNom());
		assertEquals("Le numero du joueur devrait etre 0", 0, j.getNum());
	}

	@Test
	//Pioche normale
	public void testPiocher() {
		Joueur j = new Joueur(0, "a", "R", null);
		
		j.piocher(j.getPS(), j.getDS());
		
		assertEquals("Le jouueur devrait avoir 3 cartes dans sa main", 3, j.getMain().size());
		
	}
	
	@Test
	//Pioche deux fois de suite
	public void testPiocher2() {
		Joueur j = new Joueur(0, "a", "R", null);
		
		j.piocher(j.getPS(), j.getDS());
		j.piocher(j.getPS(), j.getDS());
		
		assertEquals("Le jouueur devrait avoir 6 cartes dans sa main", 6, j.getMain().size());
		
	}
	

	@Test
	
	//Choix carte normale
	public void testChoisirCarte() {
		
		Joueur j = new Joueur(0, "a", "R", null);
		j.piocher(j.getPS(), j.getDS());
		Carte c = j.getMain().get(0);
		
		j.choisirCarte(0, j.getDS());
		
		assertEquals("La carte choisie devrait etre "+c.toString(), c, j.getCs());
	}
	
	//Choix carte impossible
		public void testChoisirCarte2() {
			
			Joueur j = new Joueur(0, "a", "R", null);
			j.piocher(j.getPS(), j.getDS());
			j.choisirCarte(-1, j.getDS());
			
			assertEquals("La carte ne devrait pas exister", null, j.getCs());
		}

}

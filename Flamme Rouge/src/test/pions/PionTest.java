package test.pions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.jeu.Joueur;
import application.pions.Pion;
import application.plateau.Cellule;

public class PionTest {

    @Test
    //Avancement normal
    public void testAvancer() {
	Cellule c1=new Cellule(0);
	Cellule c2=new Cellule(0);
	Pion p = new Pion(c1, "R", new Joueur(0, "j", "R", null)) {

	    @Override
	    public String typePion() {
		return null;
	    }
	};
	
	p.avancer(c2);
	
	assertEquals("Le pion devrait etre sur la cellule 2", c2, p.getPosition());
    }
    
    @Test
    // Si la cellule est null
    public void testAvancer2() {
	Cellule c1 = new Cellule(0);
	Pion p = new Pion(c1, "R", new Joueur(0, "j", "R", null)) {

	    @Override
	    public String typePion() {
		return null;
	    }
	};

	p.avancer(null);

	assertEquals("Le pion devrait etre sur la cellule 1", c1, p.getPosition());
    }
    
    @Test
    // Si la cellule est identique
    public void testAvancer3() {
	Cellule c1 = new Cellule(0);
	Pion p = new Pion(c1, "R", new Joueur(0, "j", "R", null)) {

	    @Override
	    public String typePion() {
		return null;
	    }
	};

	p.avancer(c1);

	assertEquals("Le pion devrait etre sur la cellule 1", c1, p.getPosition());
    }

}

package test.cartes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.cartes.Carte;

public class CarteTest {

    @Test
    //Test normal du constructeur
    public void testCarte() {
	Carte c = new Carte(3,"R") {
	    
	    @Override
	    public String typeCarte() {
		return null;
	    }
	};
	
	assertEquals("La valeur de la carte devrait etre de 3", 3, c.getValeur());
	assertEquals("La couleur de la carte devrait etre Rouge", "R", c.getCouleur());
    }
    
    @Test
    //Test valeur negative
    public void testCarte2() {
	Carte c = new Carte(-3,"R") {
	    
	    @Override
	    public String typeCarte() {
		return null;
	    }
	};
	
	assertEquals("La valeur de la carte devrait etre de 2", 2, c.getValeur());
    }
    
    @Test
    //Test valeur superieure a 9
    public void testCarte3() {
	Carte c = new Carte(13,"R") {
	    
	    @Override
	    public String typeCarte() {
		return null;
	    }
	};
	
	assertEquals("La valeur de la carte devrait etre de 9", 9, c.getValeur());
    }

}

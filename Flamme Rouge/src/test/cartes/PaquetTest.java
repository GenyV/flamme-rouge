package test.cartes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import application.cartes.Carte;
import application.cartes.CarteFatigue;
import application.cartes.CarteSprinteur;
import application.cartes.Paquet;

public class PaquetTest {

    @Test
    //Constructeur avec des cartes sprinteur plein
    public void testPaquet() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	boolean ok =true;
	String couleur="R";
	for (Carte c : p.getP()) {
	    if (!(c instanceof CarteSprinteur)) ok=false;
	    if (!c.getCouleur().equals("R")) couleur=c.getCouleur();
	}
	
	assertTrue("Il devrait n'y avoir que des cartes sprinteurs dans le paquet", ok);
	assertEquals("Toutes les cartes devraient etre rouge", "R",couleur);
    }
    
    @Test
    //Constructeur d'un paquet vide
    public void testPaquetVide() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", false);
	
	assertEquals("Le paquet devrait etre vide", 0, p.taille());
    }

    @Test
    //Test lorsque le paquet est plein
    public void testPiocherCarte() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Carte c = p.get(0);
	Carte c2 = p.piocherCarte();
	
	assertEquals("La carte piochee devrait etre la premiere du paquet", c, c2);
    }
    
    @Test
    //Test lorsque le paquet est vide
    public void testPiocherCartePaquetVide() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", false);
	Carte c = p.piocherCarte();
	
	assertEquals("La carte piochee devrait etre null", null, c);
    }

    @Test
    //Test lors de l'ajout d'une carte normale
    public void testAjouterCarte() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Carte c = new Carte(3,"R") {	    
	    @Override
	    public String typeCarte() {
		return null;
	    }
	};
	p.ajouterCarte(c);
	
	assertEquals("La taille du paquet devrait etre de 16", 16, p.taille());
	assertEquals("La derniere carte devrait etre une carte rouge de valeur 3", c, p.get(p.taille()-1));
    }
    
    @Test
    //Test lors de l'ajout d'une carte deja dans le paquet
    public void testAjouterCarte2() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Carte c = p.get(0);
	
	p.ajouterCarte(c);
	
	assertEquals("La taille du paquet devrait etre de 15", 15, p.taille());
	assertEquals("La derniere carte ne devrait pas etre la meme que la premiere", false, c==p.get(p.taille()-1));
    }

    @Test
    public void testMelanger() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Paquet p2 =new Paquet(Paquet.SPRINTEUR, "R", false);
	for (Carte c : p.getP()) p2.ajouterCarte(c);
	p.melanger();
	
	assertEquals("Les deux paquets ne devraient pas etre identiques", false, p==p2);
    }

    @Test
    //Test lorsque la carte est presente dans le paquet
    public void testRetirerCarte() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Carte c = p.get(0);

	p.retirerCarte(c);

	assertEquals("La carte ne devrait plus etre dans le paquet", false, p.getP().contains(c));
	assertEquals("Le paquet ne devrait avoir plus que 14 cartes", 14, p.taille());
    }
    
    @Test
    //Test lorsque la carte n'est pas presente dans le paquet
    public void testRetirerCarte2() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", true);
	Carte c = new Carte(3,"R") {
	    @Override
	    public String typeCarte() {
		return null;
	    }
	};

	p.retirerCarte(c);

	assertEquals("Le paquet  devrait avoir plus 15 cartes", 15, p.taille());
    }

    @Test
    //Lorsque la recharge n'est pas vide
    public void testRechargerPaquet() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", false);
	Paquet p2 = new Paquet(Paquet.SPRINTEUR, "R", true);
	
	p.rechargerPaquet(p2);
	
	assertEquals("Le premier paquet devrait avoir 15 cartes ", 15, p.taille());
	assertEquals("Le second paquet devrait etre vide", 0, p2.taille());
    }
    
    @Test
    //Lorsque la recharge est vide
    public void testRechargerPaquet2() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", false);
	Paquet p2 = new Paquet(Paquet.SPRINTEUR, "R", false);
	
	p.rechargerPaquet(p2);
	
	assertEquals("Le premier paquet devrait avoir 0 carte", 0, p.taille());
	assertEquals("Le second paquet devrait etre vide", 0, p2.taille());
    }


    @Test
    public void testAjouterCarteFatigue() {
	Paquet p = new Paquet(Paquet.SPRINTEUR, "R", false);
	CarteFatigue c = new CarteFatigue(2, "R");
	p.ajouterCarteFatigue(c);
	assertEquals("La premiere carte devrait etre une carte fatigue", c, p.get(0));
    }
}
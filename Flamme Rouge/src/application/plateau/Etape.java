package application.plateau;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import application.pions.Pion;

/**
 * @author GENY Vanessa
 *
 */
public class Etape {

    /*
     * Declaration des variables de classe
     */

    // Initialisation d'attributs statiques permettant une facilite a la
    // creation du plateau
    private static BufferedReader br;

    // Creation de la variable allant contenir la liste de toutes les cases du
    // plateau
    private LinkedList<Cellule> plateau = new LinkedList<>();

    /**
     * @param nom
     *            nom de la carte a creer
     */
    public Etape(String nom) {

	try {
	    // Liste des circuits disponibles
	    String[] circuit = new File("data/circuits/").list();

	    // On recupere le nom de chaque circuit (sans l'extension)
	    for (int i = 0; i < circuit.length; i++)
		circuit[i] = circuit[i].substring(0, circuit[i].length() - 8);

	    JComboBox<String> jcb = new JComboBox<String>(circuit);
	    // Affichage d'une fenetre de choix
	    JOptionPane.showMessageDialog(null, jcb,"Choisissez un circuit", JOptionPane.INFORMATION_MESSAGE);
	    int choix = jcb.getSelectedIndex();
	    // si l'utilisateur ferme la fenetre, le jeu s'arrete
	    if (choix == -1)
		System.exit(0);

	    // On stocke le fichier du cieurcuit choisit
	    br = new BufferedReader(new FileReader("data/circuits/" + circuit[choix] + ".circuit"));
	} catch (FileNotFoundException e) {

	    System.out.println("Circuit inconnu, circuit par defaut utilis�");
	    try {
		// Si le fichier est inconnu alors on utilise celui par defaut
		br = new BufferedReader(new FileReader("data/circuits/defaut.circuit"));

	    } catch (FileNotFoundException e1) {

		// Si il n'existe pas, le jeu s'arrete
		System.out.println("Fichier circuit inexistant");
		System.exit(0);
	    }
	} finally {
	    // On cree le circuit
	    this.creerCircuit();
	}
    }

    /**
     * Constructeur vierge pour le mode edition
     */
    public Etape() {
    }

    /**
     * methode permettant de creer le circuit a partir d'un fichier circuit
     */
    private void creerCircuit() {
	try {
	    // On lit la premiere ligne du fichier
	    String c = br.readLine();

	    // Tant que la ligne n'est pas nulle
	    while (c != null) {

		// On recupere la longueur de la ligne
		int longueur = c.length();

		// Pour chaque caractere dans la ligne
		for (int i = 0; i < longueur; i++) {

		    // On recupere la valeur numerique et on cree le circuit
		    plateau.add(new Cellule(Character.getNumericValue(c.charAt(i))));
		}

		// on lit la ligne suivante
		c = br.readLine();
	    }

	    // Si on arrive a la fin du fichier
	} catch (EOFException e) {
	    try {
		// on ferme le fichier
		br.close();
	    } catch (IOException e1) {
		System.out.println("Erreur a la fermeture du fichier circuit");
	    }
	} catch (IOException e) {
	    System.out.println("Probleme lors de la lecture du fichier circuit");
	}
    }

    @Override
    public String toString() {
	String res = "";
	for (Cellule cel : plateau) {
	    res += cel.toString() + "\n";
	    ;
	}
	return res;
    }

    /**
     * @return le plateau
     */
    public LinkedList<Cellule> getPlateau() {
	return plateau;
    }

    /**
     * Methode permettant a un pion d'occuper une case en particulier
     * 
     * @param i
     *            numero de la case a occuper
     * @param p
     *            Pion devant occuper la case
     */
    public void occuperCellule(int i, Pion p) {
	if (i < plateau.size() && i >= 0)
	    plateau.get(i).occuper(p);

    }
}
package application.plateau;

import application.pions.Pion;

/**
 * @author GENY Vanessa Classe modelisant une case d'une cellule
 */
public class Case {

    /*
     * Declaration des attributs
     */
    private boolean occupee;
    private Pion p;

    /**
     * Constructeur de la classe case, initialise la case comme etant vide
     */
    public Case() {
	this.occupee = false;
	this.p = null;
    }

    /**
     * Methode permettant de rendre une case occupee par un pion
     * 
     * @param pi
     *            pion occupant la case
     */
    public void occuper(Pion pi) {
	if (pi != null) {
	    this.occupee = true;
	    this.p = pi;
	}
    }

    /**
     * Methode permettant de liberer une case occupee par un pion
     */
    public void liberer() {
	this.occupee = false;
	this.p = null;
    }

    /**
     * @return si la case est occupee ou non
     */
    public boolean isOccupee() {
	return occupee;
    }

    /**
     * @return le pion occupant la case
     */
    public Pion getP() {
	return p;
    }

    /**
     * @param p
     *            nouveau pion occupant la case
     */
    public void setP(Pion p) {
	this.p = p;
    }

    @Override
    public String toString() {
	String res = "|";
	if (occupee == true)
	    res += p.typePion() + p.getC();
	else
	    res += "  ";
	res += "|";
	return res;
    }

}
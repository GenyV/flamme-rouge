package application.plateau;

import application.pions.Pion;


/**
 * @author GENY Vanessa
 * Classe modelisant une cellule d'un plateau
 */
public class Cellule {
    /*
     * Declaration des variables
     */
    
    //Initialisation de 3 variables de type entiere permettant de faciliter la creation d'une cellule
    public final static int NORMALE=0,MONTEE=1, DESCENTE=2, ARRIVEE=3; 
    
    //Creation d'un attribut recuperant le type de la cellule
    private int type;
    
    //Creation de deux cases internes a la cellule
    private Case gauche, droite;
    
    //Creation d'un booleen afin de recuperer la disponibilite de la cellule
    private boolean occupee;

    /**
     * Constructeur de la classe cellule
     * @param t type de la cellule
     */
    public Cellule(int t) {
	
	//Initialisation des attributs pour la creation d'une nouvelle cellule vide
	this.type=t;
	this.gauche=new Case();
	this.droite=new Case();
    }

    /*
     * Methode permettant de recuperer l'avancee maximale que le type de la cellule autorise
     * @return l'avancee maximale
     */
    public int getMaxAvancee() {
	
	//on intialise la variable de resultat a l'avancee maximale basique
	int res=9;
	
	//Si la cellule est une montee alors l'avancee de ne sera que de 5
	if (this.type==Cellule.MONTEE) res=5;
	
	//Si c'est une cellule arrivee, l'avancee sera de 0
	if (this.type==Cellule.ARRIVEE) res=0;
	
	return res;
    }
    
    /*
     * Methode permettant de recuperer l'avancee minimale que le type de la cellule autorise
     * @return l'avancee minimale
     */
    public int getMinAvancee() {
	//on intialise la variable de resultat a l'avancee minimale basique
	int res=0;
	//Si la cellule est une descente alors l'avancee sera de minimum 5
	if (this.type==DESCENTE) res=5;
	
	return res;
    }


    @Override
    public String toString() {
	String res="";
	if (type==Cellule.NORMALE) res="_";
	else if (type==Cellule.MONTEE) res=".";
	else if (type==Cellule.DESCENTE) res="=";
	else res="F";
	res+=this.droite.toString()+this.gauche.toString();


	return res;
    }

    
    /**
     * Methode permettant d'occuper une cellule
     * @param p pion devant occuper la case
     * @return true si l'occupation se passe sans soucis
     */
    public boolean occuper(Pion p) {
	
	//On initialise le booleen de retour a true
	boolean res=true;
	
	//Si la case de droite n'est pas occupee, alors on l'occupe
	if (!this.droite.isOccupee()) this.droite.occuper(p);
	
	//Sinon si la case de gauche n'est pas occupee, alors on l'occupe
	else if (!this.gauche.isOccupee()) this.gauche.occuper(p);
	
	//Sinon le booleen de retour est mit a faux puisque l'operation n'a pu se faire
	else res=false;
	
	//On appelle la methode mettant a jour la variable occupee de la classe
	this.setOccupee();
	
	//On retourne le resultat
	return res;
    }

    /**
     * Methode permettant de verifier si une des deux cases est occupee
     * @return la case occupee si au moins une des deux est occupee, la case de droite est prioritaires
     */
    public Case moitieOccupee() {
	
	//On initialise la variable de resultat a null
	Case res=null;
	
	//Si la case de droite est occupee, alors on la retourne
	if (this.droite.isOccupee()) res=this.droite;
	
	//Sinon si la case de gauche est occupee, on la retourne
	else if (this.gauche.isOccupee()) res=this.gauche;
	
	//on renvoie le resultat
	return res;
    }

    
    /**
     * Methode permettant de liberer une des deux cases en fonction d'un pion
     * @param p Pion dont on doit liberer la case
     */
    public void liberer(Pion p) {
	
	//Si le pion se trouvant sur la case de droite est celui entre en parametre, alors on appelle la methode liberer de la case de droite
	if (p==droite.getP()) droite.liberer();
	
	//Sinon meme operation pour la case de gauche
	else if (p==gauche.getP()) gauche.liberer();
	
	//On met a jour le booleen occupee de la classe
	this.setOccupee();
    }

    
    /**
     * @return la valeur de booleen occupee de la classe
     */
    public boolean isOccupee() {
	return occupee;
    }

    /**
     *  Methode permettant de verifier si la cellule est totalement occupee ou non
     */
    public void setOccupee() {
	this.occupee=(this.droite.getP()!=null && this.gauche.getP()!=null);
    }

    /**
     * @return le type de la cellule sous forme d'un entier
     */
    public int getType() {
	return type;
    }

    
    /**
     * @return la case gauche
     */
    public Case getGauche() {
	return gauche;
    }

    
    /**
     * @return la case droite
     */
    public Case getDroite() {
	return droite;
    }
}
package application.jeu;

import java.util.ArrayList;

import application.cartes.Carte;
import application.cartes.Paquet;
import application.pions.Pion;
import application.plateau.Cellule;

/**
 * @author GENY Vanessa
 *
 */
public class IA extends Joueur {

    /**
     * @param numero
     *            numero de l'ia
     * @param n
     *            nom de l'ia
     * @param c
     *            couleur de l'ia
     * @param jeu
     *            jeu en cours
     */
    public IA(int numero, String n, String c, Jeu jeu) {
	// Appel du constructeur parent
	super(numero, n, c, jeu);

	// On ferme sa fenetre joueur
	this.fj.dispose();
    }

    /*
     * (non-Javadoc)
     * 
     * @see application.jeu.Joueur#piocher(application.cartes.Paquet,
     * application.cartes.Paquet)
     */
    @Override
    public void piocher(Paquet paq, Paquet defausse) {
	// On se synchronise avec la premiere phase pour la mettre en attente
	synchronized (j.pioche) {
	    j.p1fini1 = false;
	    j.pioche.notifyAll();
	}

	// On cree un attribut indiquant l'etat des paquets
	boolean vide = false;

	// Boucle for afin de piocher 3 cartes
	for (int i = 0; i < 3; i++) {

	    // Si le paquet est vide mais que la defausse n'est pas vide, on recharge le
	    // paquet a partir de la defausse
	    if (paq.taille() == 0 && defausse.taille() > 0)
		paq.rechargerPaquet(defausse);

	    // Sinon si les deux sont vides, on met l'attribut vide a vrai
	    else if (paq.taille() == 0 && defausse.taille() == 0)
		vide = true;

	    // Si il reste des cartes
	    if (!vide) {
		// On pioche la premiere carte du paquet
		main.add(paq.piocherCarte());

		// On met a jour l'interface graphique
		this.fj.AjouterMain(i, defausse);
	    }
	}
	this.meilleureCarte(defausse);
    }

    /**
     * @param paq
     *            paquet utilise pour piocher
     */
    private void meilleureCarte(Paquet paq) {
	// On initialise une variable index a 0, ce sera l'index de la carte dans la
	// main
	int index = 0;

	// On initialise la variable stockant le pion concerne a null
	Pion pi = null;

	// Si on utilise le paquet sprinteur, alors on recupere le sprinteur
	if (paq == this.DS)
	    pi = this.spr;

	// sinon on recupere le rouleur
	else
	    pi = this.rou;

	// si le pion est sur une case montee
	if (pi.getPosition().getType() == Cellule.MONTEE) {

	    // On recupere l'avancement maximal qu'autorise la case
	    int maxAvancee = pi.getPosition().getMaxAvancee();

	    // On cree une arraylist de carte regroupant les cartes "potentielles"
	    ArrayList<Carte> temp = new ArrayList<>();

	    // Pour chaque carte dans la main du joueur
	    for (Carte c : this.main) {

		// Si la valeur de la carte est inferieure ou egale a l'avancement max autorise
		if (c.getValeur() <= maxAvancee) {

		    // On ajoute la carte au paquet temporaire
		    temp.add(c);
		}
	    }

	    // Si il n'y a aucune carte potentielle
	    if (temp.size() == 0) {

		// On choisit la plus petite carte du paquet
		int min = 9;
		int indexMin = -1;

		for (Carte c : this.main) {
		    if (c.getValeur() <= min) {
			min = c.getValeur();
			indexMin = main.indexOf(c);
		    }
		}

		index = indexMin;
		// sinon on recupere la plus grande des cartes
	    } else {
		int max = 0;
		int indexMax = -1;
		for (Carte c : temp) {
		    if (c.getValeur() >= max) {
			max = c.getValeur();
			indexMax = main.indexOf(c);
		    }
		}
		index = indexMax;
	    }
	    //Si le pion n'est pas dans une montee
	} else {
	    
	    //On recupere la carte la plus grande
	    int max = 0;
	    int indexMax = -1;
	    
	    for (Carte c : this.main) {
		if (c.getValeur() >= max) {
		    max = c.getValeur();
		    indexMax = main.indexOf(c);
		}
	    }
	    index = indexMax;
	}
	//on fait choisir la carte a l'ia
	this.choisirCarte(index, paq);
    }

}
package application.jeu;

import graphisme.FenetreAccueil;

/**
 * 
 * Classe Principale du jeu
 *
 */
public class Principale {

	public static void main(String[] args) {
		new FenetreAccueil();
	}
}
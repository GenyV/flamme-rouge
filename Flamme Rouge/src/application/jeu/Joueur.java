package application.jeu;

import java.util.ArrayList;

import application.cartes.Carte;
import application.cartes.Paquet;
import application.pions.Rouleur;
import application.pions.Sprinteur;
import graphisme.FenetreJoueur;

/*
 * Classe gerant le joueur
 */
/**
 * @author GENY Vanessa
 *
 */

public class Joueur {
	/*
	 * Creation des variables de classe
	 */
	// Variable gerant les 4 paquets du joueurs (2 pioches et 2 defausses)
	protected Paquet PS, DS, PR, DR;

	// Creation des differents attributs du joueurs (nom, numero, couleur)
	protected String nom;
	protected String couleur;
	protected int num;

	// Creation de l'attribut main (Attribut gerant la main d'un joueur une fois
	// qu'il a pioche
	protected ArrayList<Carte> main;

	// Attributs gerant les deux cartes choisies par le joueur (Carte sprinteur
	// et carte rouleur)
	protected Carte cs;
	protected Carte cr;

	// Attributs gerant les deux pions du joueur (pion Rouleur et pion
	// Sprinteur)
	protected Sprinteur spr;
	protected Rouleur rou;

	// Attribut permettant de recuperer la fenetre propre de joueur
	protected FenetreJoueur fj;
	protected Jeu j;

	/*
	 * Constructeur de la classe joueur
	 * 
	 * @param numero numero du joueur
	 * 
	 * @param n nom du joueur
	 * 
	 * @param c Couleur du joueur
	 * 
	 * @param jeu jeu en cours
	 */
	public Joueur(int numero, String n, String c, Jeu jeu) {

		// On initialise les differents attributs
		this.j = jeu;
		this.nom = n;
		this.couleur = c;
		this.num = numero;

		// Initialisation de sa main en tant que liste vide
		this.main = new ArrayList<>();

		// Creation des paquets du joueur
		this.PS = new Paquet(Paquet.SPRINTEUR, this.couleur, true);
		this.DS = new Paquet(Paquet.SPRINTEUR, this.couleur, false);
		this.PR = new Paquet(Paquet.ROULEUR, this.couleur, true);
		this.DR = new Paquet(Paquet.ROULEUR, this.couleur, false);

		// Melange des deux pioches
		this.PS.melanger();
		this.PR.melanger();

		// Creation de son interface graphique
		this.fj = new FenetreJoueur(this);
	}

	/*
	 * Methode faisant piocher le joueur
	 * 
	 * @param paq paquet dans lequel le joueur pioche
	 * 
	 * @param defausse defausse liee au paquet dans lequel le joueur pioche
	 */
	public void piocher(Paquet paq, Paquet defausse) {

		// On se synchronise avec la premiere phase pour la mettre en attente
		if (j != null) {
			synchronized (j.pioche) {
				j.p1fini1 = false;
				j.pioche.notifyAll();
			}
		}

		// On cree un attribut indiquant l'etat des paquets
		boolean vide = false;

		// Boucle for afin de piocher 3 cartes
		for (int i = 0; i < 3; i++) {

			// Si le paquet est vide mais que la defausse n'est pas vide, on
			// recharge le paquet a partir de la defausse
			if (paq.taille() == 0 && defausse.taille() > 0)
				paq.rechargerPaquet(defausse);

			// Sinon si les deux sont vides, on met l'attribut vide a vrai
			else if (paq.taille() == 0 && defausse.taille() == 0)
				vide = true;

			// Si il reste des cartes
			if (!vide) {
				// On pioche la premiere carte du paquet
				main.add(paq.piocherCarte());

				// On met a jour l'interface graphique
				this.fj.AjouterMain(i, defausse);
			}
		}
	}

	/*
	 * Methode permettant de choisir une cartes
	 * 
	 * @param index position de la carte dans la main
	 */

	public void choisirCarte(int index, Paquet defausse) {

		// La carte est stockee dans une variable
		if (index >= 0 && index < this.main.size()) {
			Carte ca = this.main.get(index);

			// On retire la carte de la main
			this.main.remove(index);

			// On ajoute les cartes restantes de la main dans la defausse
			for (Carte c : this.main) {
				defausse.ajouterCarte(c);
			}

			// On vide la main
			this.main.clear();

			// On met a jour l'interface graphique
			this.fj.detruireMain();

			// Si on a choisit pour le sprinteur
			if (defausse == this.DS) {

				// On initialise la carte sprinteur du joueur
				this.setCs(ca);

				if (j != null) {
					// On se synchronise avec la phase 2
					synchronized (j.lock3) {

						// On fait piocher pour le rouleur
						this.piocher(this.PR, this.DR);

						// On previent la phase 2
						j.lock3.notifyAll();
					}
				}

			}

			// Sinon si on a choisit pour le rouleur
			else {

				// On se synchronise avec la phase 2
				synchronized (j.lock3) {

					// On initialise la carte rouleur du joueur
					this.setCr(ca);

					// On previent la phase 2 qu'elle peut demarrer
					j.lock3.notifyAll();
				}
			}
		}

	}

	@Override
	public String toString() {
		return nom;
	}

	/**
	 * @return couleur couleur du joueur
	 */
	public String getCouleur() {
		return this.couleur;
	}

	/*
	 * Setter de l'attribut couleur
	 * 
	 * @param c nouvelle couleur du joueur
	 */
	public void setCouleur(String c) {
		this.couleur = c;
	}

	/**
	 * @return le paquet Sprinteur du joueur
	 */
	public Paquet getPS() {
		return PS;
	}

	/**
	 * @return la defausse sprinteur du joueur
	 */
	public Paquet getDS() {
		return DS;
	}

	/**
	 * @return le paquet rouleur du joueur
	 */
	public Paquet getPR() {
		return PR;
	}

	/**
	 * @return la defausse rouleur du joueur
	 */
	public Paquet getDR() {
		return DR;
	}

	/**
	 * @return la carte courante choisie pour le sprinteur
	 */
	public Carte getCs() {
		return cs;
	}

	/**
	 * @param cs
	 *            nouvelle carte sprinteur choisie par le joueur
	 */
	public void setCs(Carte cs) {
		this.cs = cs;
	}

	/**
	 * @return la carte rouleur choisie par le joueur
	 */
	public Carte getCr() {
		return cr;
	}

	/**
	 * @param cr
	 *            nouvelle carte rouleur choisie par le joueur
	 */
	public void setCr(Carte cr) {
		this.cr = cr;
	}

	/**
	 * @return le pion sprinteur du joueur
	 */
	public Sprinteur getSpr() {
		return spr;
	}

	/**
	 * @param spr
	 *            nouveau pion sprinteur du joueur
	 */
	public void setSpr(Sprinteur spr) {
		this.spr = spr;
	}

	/**
	 * @return le pion rouleur du joueur
	 */
	public Rouleur getRou() {
		return rou;
	}

	/**
	 * @param rou
	 *            nouveau pion joueur du joueur
	 */
	public void setRou(Rouleur rou) {
		this.rou = rou;
	}

	/**
	 * @return la main de cartes actuelle de joueur
	 */
	public ArrayList<Carte> getMain() {
		return main;
	}

	/**
	 * @param main
	 *            nouvelle main de cartes du joueur
	 */
	public void setMain(ArrayList<Carte> main) {
		this.main = main;
	}

	/**
	 * @return le jeu en cours
	 */
	public Jeu getJ() {
		return j;
	}

	/**
	 * @return le nom du joueur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return le numero du joueur
	 */
	public int getNum() {
		return num;
	}

	/**
	 * @return l'interface du joueur
	 */
	public FenetreJoueur getFj() {
		return this.fj;
	}

}
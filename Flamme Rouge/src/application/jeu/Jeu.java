package application.jeu;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import application.cartes.CarteFatigue;
import application.cartes.Paquet;
import application.pions.Pion;
import application.pions.Rouleur;
import application.pions.Sprinteur;
import application.plateau.Case;
import application.plateau.Cellule;
import application.plateau.Etape;
import graphisme.FenetreConfigurer;
import graphisme.FenetreEtape;
import graphisme.FenetreFin;
import graphisme.PanelPaysage;

/**
 * @author GENY Vanessa
 *
 */
public class Jeu {

    /**
     * declaration des variables de classe
     */

    // Variable du tableau des differents joueurs
    private ArrayList<Joueur> tabJ = new ArrayList<>();

    // Tableau des differentes couleurs disponibles (R=Rouge, B=Blea, V=Vert,
    // N=Noir)
    private String[] c = { "R", "B", "V", "N" };

    // Creation de l'attribut plateau, qui sera le plateau de jeu
    private Etape plateau;

    // Creation des differents paquets de carte fatigue
    private Paquet pFS = new Paquet(Paquet.FATIGUE, "SPRINTEUR", true);
    private Paquet pFR = new Paquet(Paquet.FATIGUE, "ROULEUR", true);

    // On initialise les differents booleens impactant les processus sur false
    private boolean p1fini = false, go = false, fini;
    public boolean p1fini1 = false, p2fini = false, finitour = false;

    // Creation des differents lock des processus
    protected Object lockPhase1 = new Object(), lockPhase2 = new Object(), lockAspiration = new Object(),
	    tours = new Object();
    public Object lock3 = new Object(), pioche = new Object();

    // Creation des attrivbuts de l'interface graphique du plateau
    private FenetreEtape fe;
    private PanelPaysage pp;

    // Creation d'une variable pour le nombre de tours
    private int nbTours;

    // Creation de la variable si le malus derapage est present
    private int derapage = 0;

    /**
     * @param nbJ
     *            nombre de joueurs
     * @param nbIA
     *            nombre d'IA
     */
    public Jeu(int nbJ, int nbIA) {

	// On recupere la configuration du jeu
	FenetreConfigurer.recupererConfig(derapage);

	// on initialise le nombre de tours
	this.nbTours = 0;

	// On cree le plateau
	plateau = new Etape("defaut");

	// On initialise l'attribut de fin a faux
	this.fini = false;

	// On initialise i a 0 (iterateur)
	int i = 0;

	// On cree une liste des noms des IA

	String[] nomsIA = { "IA 1", "IA 2", "IA 3", "IA 4" };
	// Tant que i est strictement inferieur au nombre de joueurs
	while (i < nbJ + nbIA) {
	    String n = "";
	    if (i < nbJ) {
		// On recupere le nom du joueur par le biais d'un JDialog
		n = JOptionPane.showInputDialog(null, "Nom du Joueur n�" + (i + 1), "Initialisation",
			JOptionPane.QUESTION_MESSAGE);

		// Si n est null (si l'utilisateur a clique sur la croix), on
		// quitte le jeu
		if (n == null)
		    System.exit(0);

		// On rajoute le joueur au tableau des joueurs
		tabJ.add(new Joueur(i + 1, n, c[i], this));
	    } else {
		n = nomsIA[i];
		tabJ.add(new IA(i + 1, n, c[i], this));
	    }
	    // On cree ses pions
	    tabJ.get(i).setSpr(new Sprinteur(plateau.getPlateau().get(i), c[i], tabJ.get(i)));
	    tabJ.get(i).setRou(new Rouleur(plateau.getPlateau().get(i), c[i], tabJ.get(i)));

	    // On place les pions sur le plateau
	    plateau.getPlateau().get(i).occuper(tabJ.get(i).getSpr());
	    plateau.getPlateau().get(i).occuper(tabJ.get(i).getRou());

	    // On incremente i
	    i++;

	}

	// On lance les differents processus
	this.deroulerJeu.start();
	this.phase1.start();
	this.phase2.start();
	this.aspiration.start();

	// On se synchronise avec le processus de deroulerJeu et on lui indique
	// qu'il
	// peut se lancer
	synchronized (this.tours) {
	    this.finitour = true;
	    this.tours.notifyAll();

	}

	// On creer l'interface graphique du plateau
	fe = new FenetreEtape(this, plateau);

	// On initialise le panelPaysage du jeu
	pp = (PanelPaysage) fe.getContentPane();

    }

    /**
     * Processus coordonnant le debut de chaque tour
     */
    Thread deroulerJeu = new Thread(new Runnable() {
	@Override
	public void run() {

	    // Tant que le jeu n'est pas fini
	    while (!Jeu.this.fini) {

		// On synchronise le processus avec l'etat actuel du tour
		synchronized (tours) {

		    // tant qu'on est pas au debut d'un tour
		    while (!Jeu.this.finitour)

			// Le processus attend
			try {
			    tours.wait();
			} catch (InterruptedException e) {
			    System.out.println("Erreur synchronisation tours(deroulerJeu)");
			}
		}

		// Si le jeu n'est pas fini
		if (!Jeu.this.fini) {

		    // On incremente le nombre de tours

		    // Synchronisation avec le processus de la premiere phase
		    synchronized (lockPhase1) {

			// On indique a la premiere phase qu'elle peut se lancer
			Jeu.this.go = true;
			lockPhase1.notifyAll();
		    }
		}

		// Si le jeu est termine
		if (Jeu.this.fini) {

		    // On classe les joueurs
		    Jeu.this.classement();

		    // On sauvegarde le resultat
		    Jeu.this.sauvegarder();
		}
	    }

	}

    });

    /*
     * Methode permettant de sauvegarder les scores
     */
    private void sauvegarder() {
	try {
	    // On cree un nouveau fichier Resultat.txt et une instance pour
	    // ecrire dedans
	    BufferedWriter bw = new BufferedWriter(new FileWriter("Resultats.txt"));

	    // Pour chaque joueur
	    for (int i = 0; i < tabJ.size(); i++) {

		// On ecrit son classement (position du tableau + 1 ) et son nom
		bw.write("Position " + (i + 1) + " : " + tabJ.get(i) + "\r");

	    }

	    // On ferme le fichier et arrete le jeu
	    bw.close();
	    this.fe.dispose();
	    for (int i = 0; i < this.tabJ.size(); i++)
		this.tabJ.get(i).getFj().dispose();
	    new FenetreFin(this);

	} catch (FileNotFoundException e) {
	    System.out.println("Fichier inexistant");
	    e.printStackTrace();
	} catch (IOException e2) {
	    System.out.println("Probleme d'E/S");
	    e2.printStackTrace();
	}

    }

    /*
     * Processus de la phase 1 (phase de pioche)
     */
    Thread phase1 = new Thread(new Runnable() {
	@Override
	public void run() {

	    // Tant que le jeu n'est pas fini
	    while (!Jeu.this.fini) {

		// On se synchronise avec le processus de deroulerJeu
		synchronized (lockPhase1) {

		    // Tant que le tour ne commence pas, on attend
		    while (!go)
			try {
			    lockPhase1.wait();
			} catch (InterruptedException e) {
			    System.out.println("Erreur lockPhase1 (phase1)");
			}
		}

		// On se synchronise avec deroulerJeu
		synchronized (tours) {

		    // On lui indique que le tour n'est pas fini
		    Jeu.this.finitour = false;
		    tours.notifyAll();
		}
		Jeu.this.nbTours++;
		// Pour chaque joueur
		for (Joueur j : tabJ) {

		    // On lui fait piocher d'abord dans le paquet Sprinteur
		    j.piocher(j.getPS(), j.getDS());

		}

		// On se synchronise avec le processus de la phase 2
		synchronized (lockPhase2) {
		    // On lui indique que la premiere phase est terminee
		    Jeu.this.p1fini = true;
		    lockPhase2.notifyAll();
		}

		// Le processus se synchronise avec lui meme pour se mettre en
		// phase d'attente
		synchronized (lockPhase1) {
		    Jeu.this.go = false;
		    lockPhase1.notifyAll();
		}
	    }
	}

    });

    /*
     * Processus gerant la seconde phase du jeu, phase d'avancement
     */
    Thread phase2 = new Thread(new Runnable() {

	@Override
	public void run() {
	    // Tant que le jeu n'est pas termine
	    while (!Jeu.this.fini) {
		// on se synchronise avec le processus de la phase 1
		synchronized (lockPhase2) {
		    // Tant que la phase 1 n'est pas terminee
		    while (!p1fini)
			// on attend
			try {
			    lockPhase2.wait();
			} catch (InterruptedException e1) {
			    System.out.println("Erreur lockPhase2 (phase2)");
			}
		}
		// Pour tous les joueurs
		for (Joueur j : tabJ) {

		    // on fait une synchronisation avec le choix des cartes
		    synchronized (lock3) {

			// Tant que le joueur n'a pas choisi ses deux cartes, on
			// attend
			while (j.getCs() == null || j.getCr() == null) {
			    try {
				lock3.wait();
			    } catch (InterruptedException e) {
				System.out.println("Erreur synchronisation lock3 (phase2)");
			    }
			}
		    }
		    // on creer une liste de reference pour la fenetre de choix
		    String[] l_cycliste = { "Sprinteur", "Rouleur" };

		    int choixC = 0;
		    // on fait apparaitre la fenetre de choix et on recupere le
		    // choix du joueur
		    if (!(j instanceof IA)) {
			new JOptionPane();
			choixC = JOptionPane.showOptionDialog(null,
				"Quel cycliste souhaitez vous faire avancer en premier ?", "Flamme Rouge",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, l_cycliste,
				l_cycliste[1]);

		    }

		    // Si le joueur a clique sur le bouton annule, le jeu
		    // s'arrete
		    if (choixC == -1)
			System.exit(0);

		    // on recupere le cycliste du joueur
		    String cycliste = l_cycliste[choixC];

		    // on initialise l'energie a 0
		    int energie = 0;

		    // Le pion concerne par l'avancement est initialise a null
		    Pion tempS = null;

		    // Si le joueur a choisi Sprinteur
		    if (cycliste.equals("Sprinteur")) {

			// on recupere son sprinteur
			tempS = j.getSpr();

			// l'energie est initialisee a la valeur de la carte
			energie = j.getCs().getValeur();

			// On effectue la mise a jour du pion par rapport a son
			// avancee si il n'est pas tombe
			if (!tempS.getATerre())
			    j.setSpr((Sprinteur) bougerPion(tempS, energie));

			// sinon on le releve
			else
			    tempS.setATerre(false);

			// On recupere le rouleur
			tempS = j.getRou();

			// On recupere son energie
			energie = j.getCr().getValeur();

			// On le fait aussi avancer si il n'est pas tombe
			if (!tempS.getATerre())
			    j.setRou((Rouleur) bougerPion(tempS, energie));

			// sinon on le releve aussi
			else
			    tempS.setATerre(false);

			// Meme processus si il a choisi le rouleur
		    } else {
			tempS = j.getRou();
			energie = j.getCr().getValeur();
			if (!tempS.getATerre())
			    j.setRou((Rouleur) bougerPion(tempS, energie));
			else
			    tempS.setATerre(false);
			tempS = j.getSpr();
			energie = j.getCs().getValeur();
			if (!tempS.getATerre())
			    j.setSpr((Sprinteur) bougerPion(tempS, energie));
			else
			    tempS.setATerre(false);
		    }

		    // On remet les cartes choisies par le joueur a null
		    j.setCr(null);
		    j.setCs(null);

		    // Si le jeu est fini alors, on arrete la boucle
		    if (Jeu.this.fini)
			break;
		}

		// On effectue le classement des joueurs par rapport a leur
		// nouvelle position
		Jeu.this.classement();

		// Si le jeu n'est pas fini
		if (!Jeu.this.fini) {

		    // On se synchronise avec le processus d'aspiration
		    synchronized (lockAspiration) {

			// On lui indique que la phase 2 est terminee
			p2fini = true;
			lockAspiration.notifyAll();
		    }
		} else {

		    // Sinon on se synchronise avec le processus DeroulerJeu
		    synchronized (tours) {
			// on lui indique que le tour est termine
			Jeu.this.finitour = true;
			Jeu.this.tours.notifyAll();
		    }
		}

	    }
	}

    });

    /*
     * Methode pour classer les joueurs
     */
    public void classement() {

	// On initialise une liste temporaire
	ArrayList<Joueur> tempJ = new ArrayList<>();

	// Pour tous les joueurs dans le tableau de joueur
	for (Joueur joueur : tabJ) {

	    // On recupere la position du sprinteur sur le plateau
	    int s1 = plateau.getPlateau().indexOf(joueur.getSpr().getPosition());

	    // On recupere la position du rouleur sur le plateau
	    int r1 = plateau.getPlateau().indexOf(joueur.getRou().getPosition());

	    // Si la liste temporaire est vide, le joueur devient
	    // automatiquement le premier
	    if (tempJ.size() == 0) {
		tempJ.add(joueur);
	    } else {
		int i = 0;
		while (i < tempJ.size()) {
		    // on recupere la position du sprinteur
		    int s2 = plateau.getPlateau().indexOf(tempJ.get(i).getSpr().getPosition());

		    // on recupere la position du rouleur
		    int r2 = plateau.getPlateau().indexOf(tempJ.get(i).getRou().getPosition());

		    // si le joueur est avant celui actuel dans la liste ou
		    // qu'il est sur la meme
		    // cellule mais a droite
		    if (Math.max(s1, r1) > Math.max(s2, r2) || (Math.max(s1, r1) > Math.max(s2, r2)
			    && plateau.getPlateau().get(Math.max(s1, r1)).getDroite().getP().getJ() == joueur)) {

			// on insere le joueur dans le tableau a la place de
			// l'autre
			tempJ.add(i, joueur);

			// on arrete la recherche
			break;
		    }
		    i++;
		}
		if (!tempJ.contains(joueur))
		    tempJ.add(tempJ.size(), joueur);
	    }

	}
	// on vide la liste des joueurs
	tabJ.clear();

	// on fait une copie de la liste temporaire dans la liste des joueurs
	for (int i = 0; i < tempJ.size(); i++)
	    tabJ.add(i, tempJ.get(i));
    }

    /*
     * Processus servant a gerer les aspirations
     */
    Thread aspiration = new Thread(new Runnable() {

	@Override
	public void run() {

	    // Tant que le jeu n'est pas fini
	    while (!Jeu.this.fini) {

		// synchronisation avec la seconde phase, tant qu'elle n'est pas
		// finie, on
		// attend
		synchronized (lockAspiration) {
		    while (!p2fini)
			try {
			    lockAspiration.wait();
			} catch (InterruptedException e) {
			    System.out.println("Erreur synchronisation lockAspiration (aspiration)");
			}
		}

		// on initialise l'index de la case la plus proche de l'arrivee
		// occupee
		int index;

		// On recupere la position du pion le plus proche de l'arrivee
		index = Math.max(plateau.getPlateau().indexOf(tabJ.get(0).getSpr().getPosition()),
			plateau.getPlateau().indexOf(tabJ.get(0).getRou().getPosition()));

		// tant que l'on est pas au debut du plateau
		while (index >= 0) {

		    // on initialise les cellules
		    Cellule actuelle = plateau.getPlateau().get(index);
		    Cellule suivante = null;
		    Cellule suivante2 = null;

		    // si l'on est pas a la derniere cellule
		    if (index + 1 < plateau.getPlateau().size())
			suivante = plateau.getPlateau().get(index + 1);

		    // si l'on est pas a l'avant derniere cellule
		    if (index + 2 < plateau.getPlateau().size())
			suivante2 = plateau.getPlateau().get(index + 2);

		    // si une des deux cases de la cellule actuelle est occupee
		    if (actuelle.moitieOccupee() != null) {

			// si on est pas a la derniere case
			if (suivante != null) {

			    // si la case suivante n'a pas d'occupant
			    if (suivante.moitieOccupee() == null) {

				// si on est pas a l'avant derniere case
				if (suivante2 != null) {

				    // si la cellule 2 cellules plus loin a des
				    // occupants
				    if (suivante2.moitieOccupee() != null) {

					// on recupere laquelle des deux cases
					// actuelles est occupee
					Case caseOccupee = actuelle.moitieOccupee();

					// on avance a la case suivante et on
					// libere la case actuelle puis on
					// applique
					// l'actualisation de la fenetre
					caseOccupee.getP().avancer(suivante);
					caseOccupee.liberer();

					// Si la case de gauche de la cellule
					// actuelle est occupee
					if (actuelle.getGauche().isOccupee() == true) {
					    // on fait passer le cycliste sur la
					    // case de droite
					    actuelle.getGauche().getP().avancer(actuelle);
					    actuelle.getGauche().liberer();
					}

					// si la case suivante2 n'est pas
					// occupee
				    } else {
					// on recupere le pion et on applique la
					// fatigue
					Pion temp = actuelle.moitieOccupee().getP();
					Jeu.this.appliquerFatigue(temp);
					// si la case gauche est aussi occupee
					if (actuelle.getGauche().isOccupee() == true) {
					    // on recupere le pion sur cette
					    // case
					    Pion temp2 = actuelle.getGauche().getP();
					    // on applique aussi la fatigue
					    Jeu.this.appliquerFatigue(temp2);
					}
				    }
				}
			    }
			}
		    }
		    index--;
		}
		// synchronisation avec deroulerJeu, le tour est fini il peut
		// reprendre
		synchronized (tours) {
		    Jeu.this.finitour = true;
		    tours.notifyAll();
		}
		// synchronisation avec lui meme pour s'arreter
		synchronized (lockAspiration) {
		    Jeu.this.p2fini = false;
		    lockAspiration.notifyAll();
		}
	    }
	}

    });

    /*
     * Methode servant a distribuer les cartes fatigues
     * 
     * @param pi Pion auquel on applique la fatigue
     */
    public void appliquerFatigue(Pion pi) {
	// si le pion est un sprinteur
	if (pi instanceof Sprinteur) {
	    // on verifie qu'il reste des cartes fatigues Sprinteurs
	    if (pFS.taille() > 0) {
		// On rajoute une carte fatigue dans le paquet Sprinteur et on
		// l'enleve du
		// paquet de carte fatigue
		pi.getJ().getPS().ajouterCarteFatigue(new CarteFatigue(2, "FATIGUE"));
		pFS.retirerCarte(pFS.get(pFS.taille() - 1));
	    }
	}

	else {
	    // sinon si c'est un rouleur
	    if (pi instanceof Rouleur) {
		// on verifie qu'il reste des cartes fatigues Rouleurs
		if (pFR.taille() > 0) {
		    // On rajoute une carte fatigue dans le paquet Rouleur et on
		    // l'enleve du paquet
		    // de carte fatigue
		    pi.getJ().getPR().ajouterCarteFatigue(new CarteFatigue(2, "FATIGUE"));
		    pFR.retirerCarte(pFR.get(pFR.taille() - 1));
		}
	    }

	}
    }

    /*
     * Methode pour bouger les pions
     * 
     * @param p Pion a bouger
     * 
     * @param energie nombre de coups disponible pour le pion
     */

    public Pion bougerPion(Pion p, int energie) {
	// on verfie que l'energie ne depasse pas le maximum autorise par la
	// case
	if (energie > p.getPosition().getMaxAvancee())
	    energie = p.getPosition().getMaxAvancee();

	// meme operation avec le minimum
	if (energie < p.getPosition().getMinAvancee())
	    energie = p.getPosition().getMinAvancee();

	int i = 0;
	// tant qu'on a pas avance de la valeur de la carte
	while (i < energie) {
	    // on recupere la position actuelle du sprinteur
	    int index = plateau.getPlateau().indexOf(p.getPosition());
	    Cellule actuelle = plateau.getPlateau().get(index);
	    // on incremente l'avancee de 1
	    i++;
	    // si le joueur n'est pas a la derniere case (=arrivee)
	    if (actuelle.getType() != Cellule.ARRIVEE) {

		// Si il y a le malus de derapage
		if (this.derapage == 1) {

		    // Si on est dans une descente
		    if (actuelle.getType() == Cellule.DESCENTE) {

			// On genere une valeur aleatoire
			int hasard = (int) (Math.random() * 100);

			// Si elle est inferieure a 10, on tombe et on arrete son tour
			if (hasard < 10) {
			    p.setATerre(true);
			    break;
			}

		    }
		}
		// on recupere la case suivante
		Cellule suivante = plateau.getPlateau().get(index + 1);
		// si la case suivante n'est pas occupee
		if (!suivante.isOccupee()) {
		    // on libere la case actuelle
		    p.getPosition().liberer(p);
		    // on avance a la suivante
		    p.avancer(suivante);

		} else {
		    // si la case suivante est occupee mais que l'on a encore
		    // des "coups" a jouer
		    if (i < energie) {
			// nombre de coups restants
			int reste = energie - i + 1;
			// case plus avancee qu'on teste
			int prochaineCase = 1;
			// etat de la recherche
			boolean ok = false;
			// tant qu'on a pas trouve de case et qu'on a pas epuise
			// nos coups
			while (prochaineCase <= reste && !ok) {
			    // si on trouve une case libre, ok devient vrai
			    if (!plateau.getPlateau().get(index + prochaineCase).isOccupee())
				ok = true;

			    else {
				// sinon on incremente
				prochaineCase++;
			    }
			}
			// si aucune case n'est libre, alors on sort du if
			if (!ok)
			    break;

			else {
			    // on libere la case occupee par le sprinteur
			    p.getPosition().liberer(p);
			    // on le fait avancer
			    p.avancer(plateau.getPlateau().get(index + prochaineCase));
			    // on met a jour l'affichage
			    pp.modifier();
			    // on met a jour le nombre de coups restants
			    i += prochaineCase - 1;
			}
		    }
		}
	    }
	    // si l'on est a l'arrivee
	    if (p.getPosition().getType() == Cellule.ARRIVEE) {
		// le jeu s'arrete
		this.fini = true;
		break;
	    }
	    // si on avance du nombre maximal de case autorisee (par ex 5 dans
	    // une montee),
	    // la boucle s'arrete
	    if (i >= p.getPosition().getMaxAvancee())
		break;

	}
	return p;
    }

    /**
     * getter de l'attribut fini
     * 
     * @return fini booleen indiquant l'etat actuel du jeu
     */
    public boolean getFini() {
	return this.fini;
    }

    /**
     * @return le panelpaysage actuel
     */
    public PanelPaysage getPp() {
	return this.pp;
    }

    /**
     * @return le tableau de joueur
     */
    public ArrayList<Joueur> getTabj() {
	return this.tabJ;
    }

    /**
     * @return le plateau
     */
    public Etape getPlateau() {
	return this.plateau;
    }

    /**
     * @return le nombre de tours
     */
    public int getNbTours() {
	return this.nbTours;
    }
}
package application.cartes;

import java.util.ArrayList;
import java.util.Collections;

public class Paquet {

    /*
     * Declaration des variables
     */
    // Constantes pour le type de la carte
    public final static int SPRINTEUR = 1, ROULEUR = 2, FATIGUE = 3;

    // ArrayList representant le paquet
    private ArrayList<Carte> p;

    // type du paquet
    private int type;

    /**
     * @param t
     *            type du paquet
     * @param couleur
     *            couleur du paquet
     * @param rempli
     *            si le paquet doit etre rempli ou vide
     */
    public Paquet(int t, String couleur, boolean rempli) {

	// on initialise le paquet de cartes
	p = new ArrayList<Carte>();

	// on initialise le type du paquet
	this.type = t;

	// si le paquet doit etre rempli
	if (rempli) {

	    // Switch en fonction du type du paquet
	    switch (this.type) {

	    // On cree les paquets en fonction de leur specificite
	    case (SPRINTEUR):

		// On commence par initialiser la plus petite valeur des cartes a 2
		int valSprinteur = 2;

		// Boucle for pour creer les 15 cartes
		for (int i = 1; i <= 15; i++) {

		    // On ajoute la carte au paquet
		    this.p.add(new CarteSprinteur(valSprinteur, couleur));

		    // A chaque fois qu'on atteint 3 cartes, on incremente la valeur des cartes
		    if (i % 3 == 0) {
			// si la valeur des cartes est inferieure a 5, alors on incremente de 1
			if (valSprinteur < 5)
			    valSprinteur++;

			// sinon la valeur passe directement a 9
			else
			    valSprinteur = 9;
		    }
		}
		break;
	    // Dans le cas des rouleurs
	    case (ROULEUR):

		// On commence la valeur des cartes a 3
		int valRouleur = 3;

		// On fait une boucle for pour creer les 15 cartes
		for (int i = 1; i <= 15; i++) {

		    // On ajoute la carte rouleur au paquet avec sa valeur et sa couleur
		    this.p.add(new CarteRouleur(valRouleur, couleur));

		    // Des qu'on atteint 3 cartes, on incremente la valeur des cartes de 1
		    if (i % 3 == 0) {
			valRouleur++;
		    }
		}
		break;
		
	    // Dans le cas des cartes fatigues
	    case (FATIGUE):

		// On cree un paquet de 60 cartes
		for (int i = 0; i < 60; i++)
		    p.add(new CarteFatigue(2, couleur));
	    }
	}
    }

    /**
     * @return la carte piochee
     */
    public Carte piocherCarte() {
	
	//On intialise la carte a null
	Carte c = null;
	
	//si le paquet n'est pas vide
	if (p.size() > 0) {
	    
	    //On prend la premiere carte du paquet
	    c = p.get(0);
	    
	    //Et on la retire de celui-ci
	    this.retirerCarte(c);
	}
	
	//On retourne la carte
	return c;

    }

    /**
     * @param c Carte a ajouter au paquet
     */
    public void ajouterCarte(Carte c) {
	
	//Si la carte n'est pas deja dans le paquet
	if (!p.contains(c))
	//On ajoute la carte au paquet
	p.add(c);
    }

    /**
     * Methode pour melanger le paquet
     */
    public void melanger() {
	Collections.shuffle(p);
    }

    /**
     * @param c carte a retirer du paquet
     */
    public void retirerCarte(Carte c) {
	//Si le paquet contient la carte, alors on l'enleve
	if (p.contains(c)) p.remove(c);
    }

    
    /**
     * @param recharge Paquet servant de recharge au paquet actuel (this)
     */
    public void rechargerPaquet(Paquet recharge) {
	
	//Pour chaque carte dans le paquet
	for (Carte c : recharge.p) {
	    this.ajouterCarte(c);
	}
	recharge.getP().clear();
	this.melanger();
    }

    /**
     * @return la taille du paquet
     */
    public int taille() {
	return p.size();
    }

    @Override
    public String toString() {
	String res = "";
	for (Carte cs : this.p) {
	    res += cs + "\n";
	}
	return res;
    }

    /**
     * @param index index de la carte a recuperer
     * @return la carte a recuperer
     */
    public Carte get(int index) {
	return p.get(index);
    }

    /**
     * @param carteFatigue permet d'ajouter une carte fatigue au paquet
     */
    public void ajouterCarteFatigue(CarteFatigue carteFatigue) {
	
	//on recupere un index aleatoire dans le paquet
	int index = (int) (Math.random() * this.p.size());
	
	//on ajoute la carte
	p.add(index, carteFatigue);

    }
    
    /*
     * Getter de l'ArrayList
     * SEULEMENT UTILE POUR LES TEST
     */
    public ArrayList<Carte> getP(){
	return this.p;
    }
}
package application.cartes;

/**
 * @author GENY Vanessa
 * Classe gerant les cartes sprinteurs heritee de la superclasse carte
 */
public class CarteSprinteur extends Carte {


    /**
     * @param val valeur de la carte
     * @param c couleur de la carte
     */
    public CarteSprinteur(int val, String c) {
	//Appel du constructeur de la classe Carte
	super(val,c);
    }

    @Override
    public String typeCarte() {
	return "Carte Sprinteur";
    }

}
package application.cartes;


/**
 * @author GENY Vanessa
 * Classe gerant les cartes fatigues heritee de la superclasse carte
 */
public class CarteFatigue extends Carte {

    /**
     * @param val valeur de la carte
     * @param c couleur de la carte
     */
    public CarteFatigue(int val, String c) {

	//Appel du constructeur de la classe Carte
	super(val, c);
    }

    @Override
    public String typeCarte() {
	return "Carte Fatigue";
    }

}
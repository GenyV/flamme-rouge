package application.cartes;

public abstract class Carte {


    /**
     * Declaration des variables
     */

    protected int valeur;
    protected String couleur;


    /**
     * @param val valeur de la carte
     * @param c couleur de la carte
     */
    public Carte(int val, String c) {
	if (val<2) val=2;
	if (val>9) val=9;
	this.valeur=val;
	this.couleur=c;
    }

    /**
     * @return type de la carte
     */
    public abstract String typeCarte();


    @Override
    public String toString() {
	return (this.typeCarte()+" d'une valeur de "+this.valeur);
    }

    /**
     * @return la valeur de la carte
     */
    public int getValeur() {
	return this.valeur;
    }
    
    /**
     * @return la couleur de la carte
     */
    public String getCouleur() {
	return this.couleur;
    }
}
package application.cartes;

/**
 * @author GENY Vanessa
 * Classe gerant les cartes rouleurs heritee de la superclasse carte
 */
public class CarteRouleur extends Carte {

    /**
     * @param val valeur de la carte
     * @param c couleur de la carte
     */
    public CarteRouleur(int val, String c) {
	//Appel du constructeur de la classe Carte
	super(val, c);
    }

    @Override
    public String typeCarte() {
	return "Carte Rouleur";
    }

}
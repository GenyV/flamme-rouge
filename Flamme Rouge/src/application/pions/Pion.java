package application.pions;

import application.jeu.Joueur;
import application.plateau.Cellule;

/**
 * @author GENY Vanessa
 *
 */
public abstract class Pion {
    /*
     * Creation des variables de classe
     */

    // Variable qui est la position du pion sur le plateau de jeu
    private Cellule position;

    // Couleur du pion
    private String c;

    // Joueur proprietaire du pion
    private Joueur j;
    
    //Booleen pour le derapage
    private boolean aTerre=false;

    /**
     * @param pos
     *            position du pion
     * @param col
     *            couleur du pion
     * @param jou
     *            joueur proprietaire
     */
    public Pion(Cellule pos, String col, Joueur jou) {
	this.position = pos;
	this.c = col;
	this.j = jou;
    }

    /**
     * @return la position du pion
     */
    public Cellule getPosition() {
	return position;
    }

    /**
     * @param position
     *            position du pion
     */
    public void setPosition(Cellule position) {
	this.position = position;
    }

    /**
     * @return la couleur du pion
     */
    public String getC() {
	return c;
    }

    /**
     * Methode faisant avancer le pion dans une cellule
     * 
     * @param suivante
     *            Cellule ou le pion doit avancer
     */
    public void avancer(Cellule suivante) {

	//Si la cellule suivante n'est pas null
	if (suivante != null) {
	    // Changement de la positon du pion
	    this.position = suivante;

	    // On occupe la cellule suivante
	    suivante.occuper(this);

	    // On met a jour l'interface graphique
	    if (j.getJ() != null)
		j.getJ().getPp().modifier();
	}

    }

    /**
     * @return le type de pion
     */
    public abstract String typePion();

    /**
     * @return le joueur proprietaire du pion
     */
    public Joueur getJ() {
	return j;
    }
    
    /*
     * @return si le pion est tombe ou non
     */
    public boolean getATerre() {
	return this.aTerre;
    }
    
    /*
     * Setter de l'attribut aTerre
     */
    public void setATerre(boolean b) {
	this.aTerre=b;
    }
}
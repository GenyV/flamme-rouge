package application.pions;
import application.jeu.Joueur;
import application.plateau.Cellule;

/**
 * @author GENY Vanessa
 * 
 */
public class Rouleur extends Pion {

	/*
     * (non-Javadoc)
     * 
     * @see application.pions.Pion#Pion()
     */
	public Rouleur(Cellule pos, String c, Joueur j) {
		super(pos, c,j);
	}

	/* (non-Javadoc)
	 * @see application.pions.Pion#typePion()
	 */
	@Override
	public String typePion() {
		return "R";
	}

}